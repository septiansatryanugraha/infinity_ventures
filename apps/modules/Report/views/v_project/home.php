<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="box-header">
                <div class="search-form" style="">
                    <div class="form-group ">
                        <label class="control-label">Filter</label>
                    </div>
                    <form id="form-print" method="POST" action="<?= base_url("print-report-project"); ?>" target="_blank">
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Created Date</label>
                            <div class="col-sm-6">
                                <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block; background: #FFF;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                                <span>s/d</span>
                                <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block; background: #FFF;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="form-group">  
                            <label class="col-sm-2 control-label"></label>       
                            <div class="col-sm-6">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="all_date" name="all_date" checked/>All Date
                                </label>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="box-footer">
                            <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Show</button>
                            <button style="margin-top: 13px" type="submit" class="btn btn-success btn-danger"><i class="fa fa-print"></i> Print</button>
                        </div>
                    </form>
                    <div class="box-footer"><br></div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="vertical-align: middle;">#</th>
                                <th class="text-center" style="vertical-align: middle;">Category</th>
                                <th class="text-center" style="vertical-align: middle;">Name</th>
                                <th class="text-center" style="vertical-align: middle;">Currency</th>
                                <th class="text-center" style="vertical-align: middle;">Allocation</th>
                                <th class="text-center" style="vertical-align: middle;">Limit</th>
                                <th class="text-center" style="vertical-align: middle;">Remain</th>
                                <th class="text-center" style="vertical-align: middle;">Price Token</th>
                                <th class="text-center" style="vertical-align: middle;">TGE</th>
                                <th class="text-center" style="vertical-align: middle;">Vesting</th>
                                <th class="text-center" style="vertical-align: middle;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        // untuk datepicker
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });
    });
    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[10, 50, 75, 100, 150, -1], [10, 50, 75, 100, 150, "All"]],
            "bSort": false,
            "searching": false,
            "lengthChange": false,
            "pageLength": 10,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?= base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sInfoPostFix": "",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('ajax-report-project') ?>",
                "type": "POST",
                data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $('#form-print').submit(function (e) {
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            if (document.getElementById("all_date").checked == false) {
                var tanggal_awal = $("#tanggal_awal").val();
                if (tanggal_awal.length == 0) {
                    error++;
                    message = "Transaction Date is required.";
                }
                var tanggal_akhir = $("#tanggal_akhir").val();
                if (tanggal_akhir.length == 0) {
                    error++;
                    message = "Transaction Date is required.";
                }
            }
        }
        if (error == 0) {
            return true;
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>