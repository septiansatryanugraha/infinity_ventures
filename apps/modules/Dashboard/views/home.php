<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #sertifikasi {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    } 
</style>

<section class="content">
    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><span class="count"><?= $member ?></span></h3>
                    <p>Member</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="<?= base_url('master-member'); ?>" class="ajaxify small-box-footer klik">See data <i class="fa fa-arrow-circle-right"></i></a> 
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><span class="count"><?= $project ?></span></h3>
                    <p>Project</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="<?= base_url('master-project'); ?>" class="ajaxify small-box-footer klik">See data <i class="fa fa-arrow-circle-right"></i></a> 
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><span class="count"><?= $transaction ?></span></h3>
                    <p>Transaction</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="<?= base_url('management-transaction'); ?>" class="ajaxify small-box-footer klik">See data <i class="fa fa-arrow-circle-right"></i></a> 
            </div>
        </div>
    </div>
</section>
<br>

<!-- highchart -->
<script src="<?= base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?= base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?= base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
<script>
    // Animasi angka bergerak dashboard
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

    //untuk load data table ajax 
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        //datatables
        table = $('#tableku').DataTable({
            "bSort": false,
            "scrollX": true,
            oLanguage: {
                "sSearch": "<i class='fa fa-search fa-fw'></i> Cari : ",
                "sEmptyTable": "No data found in the server",
                sProcessing: "<img src='<?php base_url(); ?>assets/dist/img/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Tidak ada data di server",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            }
        });
    });
</script>