<div class="footer">
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <center><div class="copyright">Copyright &copy; <?= date("Y") ?> Infinity Ventures</div></center>
                <!-- /span12 --> 
            </div>
            <!-- /row --> 
        </div>
        <!-- /container --> 
    </div>
    <!-- /footer-inner --> 
</div>
<!-- /footer --> 

<script type="text/javascript" src="<?= base_url(); ?>assets/web/public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript" src="<?= base_url(); ?>assets/web/public/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?= base_url(); ?>assets/web/js/jquery-2.2.3.min.js"></script> 
<script type="text/javascript" src="<?= base_url() . 'assets/web/dataTable/js/dataTables.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url() . 'assets/web/datatable/js/dataTables2.min.js' ?>"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/web/toastr/toastr.js"></script>
<script src="<?= base_url(); ?>assets/web/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tabelku').DataTable({
            oLanguage: {
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            }
        });
    });
</script>