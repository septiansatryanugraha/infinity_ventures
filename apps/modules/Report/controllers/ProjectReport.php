<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectReport extends AUTH_Controller
{
    const __tableName = 'tbl_project';
    const __tableId = 'id_project';
    const __folder = 'v_project/';
    const __kode_menu = 'report-project';
    const __title = 'Report Project';
    const __model = 'M_ProjectReport';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
        $this->load->model('M_project');
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajaxList()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_ProjectReport->getData(1, [], $filter);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $vestingPrecentage = $brand->vesting_precentage * 100;
            $expVestingPrecentage = explode('.', $vestingPrecentage);
            if (isset($expVestingPrecentage[1])) {
                $vestingPrecentage = number_format($vestingPrecentage, 1, ".", ",");
            }
            $clift = (strlen($brand->clift_amount) > 0) ? 'and Clift ' . $brand->clift_amount . ' ' . $brand->clift : '';

            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->category;
            $row[] = $brand->name;
            $row[] = $brand->currency;
            $row[] = '<div style="text-align: right;">' . number_format($brand->allocation, 0, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->allocation_limit_member, 0, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->allocation_remain, 0, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->price_token, 3, ".", ",") . '</div>';
            $row[] = $brand->tge_precentage * 100 . ' % ' . $clift;
            $row[] = $vestingPrecentage . ' % for ' . number_format($brand->vesting_amount, 0, ".", ",") . ' ' . $brand->vesting;
            $row[] = $brand->status;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function printReport()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak " . self::__title;
        if ($access->menuview == 0) {
            echo "<script>alert('You don't have access.'); window.close();</script>";
        } else {
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = array(
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => isset($allDate) ? 1 : 0,
            );

            $resultData = $this->M_ProjectReport->getData(1, [], $filter);
            if (!empty($resultData)) {
                $data['filter'] = $filter;
                $data['resultData'] = $resultData;
                $data['filename'] = 'report_project_' . date('YmdHis');

                $this->load->view(self::__folder . '/print', $data);
            } else {
                echo "<script>alert('Project list is empty.'); window.close();</script>";
            }
        }
    }
}
