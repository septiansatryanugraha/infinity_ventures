<?php
include('header.php');

?>
<style type="text/css">
    #btn_loading { display: none; }
    .qrcode {
        margin-left: -20px;
        width: 200px;
        margin-top:-10px; 
    }
    .field-icon {
        float: left;
        margin-left: 90%;
        margin-top: -40px;
        font-size: 17px;
        position: relative;
        z-index: 999;
        color: #ccc;
    }
    @-webkit-keyframes placeHolderShimmer {
        0% {
            background-position: -468px 0;
        }
        100% {
            background-position: 468px 0;
        }
    }
    @keyframes placeHolderShimmer {
        0% {
            background-position: -468px 0;
        }
        100% {
            background-position: 468px 0;
        }
    }
    .content-placeholder {
        display: inline-block;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: forwards;
        animation-fill-mode: forwards;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-name: placeHolderShimmer;
        animation-name: placeHolderShimmer;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: -webkit-gradient(linear, left top, right top, color-stop(10%, #eeeeee), color-stop(18%, #dddddd), color-stop(33%, #eeeeee));
        background: -webkit-linear-gradient(left, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        -webkit-background-size: 800px 104px;
        background-size: 800px 104px;
        height: inherit;
        position: relative;
    }
    .gambar-project{
        width: 200px;
        height: 200px !important;
        object-fit: cover;
    }
    ul, li {list-style:none; margin:auto; padding:0px;}
    h2, h3 {margin:0px; padding:5px;}
    .recent_post {margin:20px; padding:5px; border:solid 2px #eeeeee}
    .recent_post li a{list-style:none; text-decoration:none; font-family:Arial, Helvetica, sans-serif;color:#333; padding:5px 0px 5px 0px;  display:table; width:100%;}
    .recent_post li a h3 {font-size:18px; font-weight:700;}
    .recent_post li a img {float:left; padding:2px; margin:5px; width:80px; height:80px;}
    .recent_post li a .date {text-align:left; font-size:12px}
    .recent_post li a:hover {background:#0066FF; color:#ffffff;}
    .timeline-item {
        background-color: #fff;
        /* border: 1px solid;*/
        border-color: #e5e6e9 #dfe0e4 #d0d1d5;
        border-radius: 3px;
        padding: 12px;
        margin: 0 auto;
        max-width: 90%;
        height: 110px;
    }
    @keyframes placeHolderShimmer {
        0%{ background-position: -468px 0; }
        100%{ background-position: 468px 0; }
    }
    .animated-background {
        animation-duration: 1.5s;
        animation-fill-mode: forwards;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
        animation-name: placeHolderShimmer;
        background: #f6f7f8;
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        background-size: 800px 104px;
        height: 96px;
        position: relative;
    }
    .background-masker {
        background: #fff;
        position: absolute;
    }
    .background-masker.header-top,
    .background-masker.header-bottom,
    .background-masker.subheader-bottom {
        top: 0;
        left: 40px;
        right: 0;
        height: 10px;
    }
    .background-masker.header-left,
    .background-masker.subheader-left,
    .background-masker.header-right,
    .background-masker.subheader-right {
        top: 10px;
        left: 40px;
        height: 8px;
        width: 10px;
    }
    .background-masker.header-bottom {
        top: 18px;
        height: 6px;
    }
    .background-masker.subheader-left,
    .background-masker.subheader-right {
        top: 24px;
        height: 6px;
    }
    .background-masker.header-right,
    .background-masker.subheader-right {
        width: auto;
        left: 35%;
        right: 0;
    }
    .background-masker.subheader-right {
        left: 45%;
    }
    .background-masker.subheader-bottom {
        top: 30px;
        height: 10px;
    }
    .background-masker.content-top,
    .background-masker.content-second-line,
    .background-masker.content-third-line,
    .background-masker.content-second-end,
    .background-masker.content-third-end,
    .background-masker.content-first-end {
        top: 40px;
        left: 0;
        right: 0;
        height: 6px;
    }
    .background-masker.content-top {
        height:60px;
    }
    .background-masker.content-first-end,
    .background-masker.content-second-end,
    .background-masker.content-third-end{
        width: auto;
        left: 30%;
        right: 0;
        top: 60px;
        height: 8px;
    }
    .background-masker.content-second-line  {
        top: 68px;
    }
    .background-masker.content-second-end {
        left: 80%;
        top: 74px;
    }
    .background-masker.content-third-line {
        top: 82px;
    }
    .background-masker.content-third-end {
        left: 90%;
        top: 88px;
    }
</style>
<div class="container"><br/>
    <div class="row">
        <?php if (!$this->session->userdata('status') == 1) : ?>
            <div class="col-md-8">
                <div class="widget ">
                    <div class="widget-content" style="max-height: 1000px; overflow-y: scroll;">
                        <div id="load_data"></div>
                        <div id="load_data_message"></div>
                    </div>
                </div>
            </div>
        <?php else: ?> 
            <div class="col-md-8">
                <div class="widget ">
                    <div class="widget-content" style="max-height: 1000px; overflow-y: scroll;">
                        <div id="load_data3"></div>
                        <div id="load_data_message3"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!$this->session->userdata('status') == 1) : ?>
            <div class="col-md-4">
                <div class="widget ">
                    <div class="widget-content">
                        <div class="account-container">
                            <div class="content clearfix">
                                <form action="<?= base_url('login-user'); ?>" method="post" id="FormLogin">
                                    <h3><center>Login Member</center></h3>
                                    <p><center>Please provide your details</center></p>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="email" class="form-control signup-input" id="email" placeholder="Email" name="email">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <div class="form-group">      
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                            <input class="form-control signup-input2" name="password" type="password" id="password-field" placeholder="Password">
                                        </div>
                                    </div>
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> 
                                    <a href="<?= site_url(); ?>register-member.html"><p>Register Account ?</p></a>
                                    <div class="login-actions">
                                        <br>
                                        <div id='btn_loading'></div>
                                        <div id="hilang">
                                            <button type="submit" id="btnSignUp" class="button btn btn-success btn-large pull-right">
                                                <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login
                                            </button>
                                        </div> 
                                        <div id="buka">
                                            <button type="submit" id="btnSignUp" class="button btn btn-success btn-large pull-right">
                                                <i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login
                                            </button>
                                        </div>
                                    </div> 
                                </form>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        <?php else: ?> 
            <div class="col-md-4">
                <div class="widget ">
                    <div class="widget-content" style="max-height: 640px; overflow-y: scroll;">
                        <h3><center>My Investment</center></h3>
                        <div id="load_data2"></div>
                        <div id="load_data_message2"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php
    $no = 0;
    foreach ($DetailOfProject as $row): $no++;

        ?>
        <div class="modal fade" id="exampleModal<?= $row->id_project; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="container" id="newContain">
                        <form  method="POST">
                            <input type="hidden" name="id_project" value="<?= $row->id_project ?>">
                            <input type="hidden" name="allocation" value="<?= $row->allocation_limit_member ?>">
                            <input type="hidden" name="id_user" value="<?= $this->session->userdata('id') ?>">
                            <div class="modal-body scroll">
                                <div class="row" style="overflow-y : auto;">
                                    <div class="col-sm-2">
                                        <img class="thumbnail" src="<?= base_url($row->image) ?>" style="width:180px;">
                                    </div>
                                    <div class="col-sm-4">
                                        <p><?php echo $row->description ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="control-group col-md-3">                     
                                        <label class="control-label" for="firstname">Allocation Contribution</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="allocation_contribution">
                                        </div>        
                                    </div> 
                                    <div class="control-group col-md-3">                     
                                        <label class="control-label" for="firstname">Allocation + fee</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="allocation_fee">
                                        </div>        
                                    </div>
                                    <!-- <div class="control-group col-md-2">                     
                                        <label class="control-label" for="firstname">TXID BSCSCAN.COM</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="txid">
                                        </div>        
                                    </div> -->
                                </div>
                                <div class="row"><div class="col-md-2"></div> 
                                    <div class="col-md-2"><h3>Payment Address</h3></div> 
                                    <div class="col-md-2"></div>   
                                </div>
                                <div class="row">
                                    <div class="col-md-2"></div>   
                                    <div class="col-md-2">                     
                                        <img class="qrcode" src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=0x9F17cC7C8A11fee35A9c0C35c484EcFdA086Efe1" /> 
                                    </div>
                                    <div class="col-md-2"></div> 
                                </div>
                                <div class="row">
                                    <div class="control-group col-md-1"></div> 
                                    <div class="control-group col-md-4"><div class="controls"><div class="input-group"><span tooltip="Copy Link" class="input-group-addon" onclick="myFunction2('0x9F17cC7C8A11fee35A9c0C35c484EcFdA086Efe1')"><i class="fa fa-clone" style="cursor:pointer;" aria-hidden="true"></i></span><input type="text" class="form-control" id="myInput20x9F17cC7C8A11fee35A9c0C35c484EcFdA086Efe1" value="0x9F17cC7C8A11fee35A9c0C35c484EcFdA086Efe1" disabled></div></div></div>
                                    <div class="control-group col-md-1"></div> 
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button  class="btn btn-primary simpan"><i class=" fa fa-paper-plane"></i> Apply</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class=" fa fa-remove"></i> Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php include('footer.php'); ?>
<script type="text/javascript" >

    function myFunction2(value) {
        /* Get the text field */
        var copyText = document.getElementById("myInput2" + value);

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(copyText.value);
        toastr.success(copyText.value, 'Link berhasil di copy', {timeOut: 5000}, toastr.options = {
            "closeButton": true});
    }

    function formValidation(oEvent) {
        oEvent = oEvent || window.event;
        var t1ck = true;
        var msg = " ";

        if (document.getElementById("email").value.length < 1) {
            t1ck = false;
            msg = msg + "Email is required";
        }
        if (document.getElementById("password-field").value.length < 1) {
            t1ck = false;
            msg = msg + "Password is required";
        }
        if (t1ck) {
            document.getElementById("btnSignUp").disabled = false;
        } else {
            document.getElementById("btnSignUp").disabled = true;
        }
    }

    function resetForm() {
        document.getElementById("btnSignUp").disabled = true;
        var frmMain = document.forms[0];
        frmMain.reset();
    }

<?php if (!$this->session->userdata('status') == 1) : ?>
        window.onload = function () {
            var btnSignUp = document.getElementById("btnSignUp");
            var email = document.getElementById("email");
            var password = document.getElementById("password-field");
            var t1ck = false;
            document.getElementById("btnSignUp").disabled = true;
            email.onkeyup = formValidation;
            password.onkeyup = formValidation;
        }
<?php endif; ?>

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    $(function () {
        $("#buka").hide();
        $('#FormLogin').submit(function (e) {
            e.preventDefault();
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#hilang").hide();
                    $("#btn_loading").html("<button class='button btn btn-success btn-large pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
                    $("#btn_loading").show();
                },
                url: $(this).attr('action'),
                type: "POST",
                cache: false,
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {
                    if (json.status == true) {
                        $("#btn_loading").hide();
                        $("#buka").show();
                        toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                        window.location = json.url_home;
                    } else {
                        $("#btn_loading").hide();
                        $("#hilang").show();
                        toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                }
            });
        });
    });


    // fungsi ajax load project 1
    $(document).ready(function () {
        var limit = 6;
        var start = 0;
        var action = 'inactive';
        function lazzy_loader(limit) {
            var output = '';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            $('#load_data_message').html(output);
        }

        lazzy_loader(limit);

        function load_data(limit, start) {
            $.ajax({
                url: "<?= base_url('load-project'); ?>",
                method: "POST",
                data: {limit: limit, start: start},
                cache: false,
                success: function (data) {
                    if (data == '') {
                        $('#load_data_message').html("");
                        action = 'active';
                    } else {
                        $('#load_data').append(data);
                        $('#load_data_message').html("");
                        action = 'inactive';
                    }
                }
            })
        }

        if (action == 'inactive') {
            action = 'active';
            load_data(limit, start);
        }

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive') {
                lazzy_loader(limit);
                action = 'active';
                start = start + limit;
                setTimeout(function () {
                    load_data(limit, start);
                }, 1000);
            }
        });
    });

    // fungsi ajax add project 
    $(document).on("click", ".add-project1", function () {
        var id_project = $(this).attr("data-id");

        swal({
            title: "Peringatan",
            text: "Yakin memilih project ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: "POST",
                url: "<?= base_url('cek-apply-project'); ?>",
                data: "id_project=" + id_project,
                beforeSend: function () {
                    swal({
                        imageUrl: "<?= base_url(); ?>assets/web/images/ajax-loader.gif",
                        title: "Please wait..",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        swal("Success", result.pesan, "success");
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                }
            });
        });
    });
</script>
<script type="text/javascript">
    // fungsi ajax add bid project
    $(".simpan").click(function () {
        var error = 0;
        var message = "";
        var data = $("#newContain>form").serialize();
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    swal({
                        imageUrl: "<?= base_url(); ?>assets/web/images/ajax-loader.gif",
                        title: "Please wait..",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                url: '<?= base_url('api-transaction-buy-project'); ?>',
                type: "POST",
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    swal("Success", result.pesan, "success");
                    setTimeout(location.reload.bind(location), 800);
                } else {
                    swal("Warning", result.pesan, "warning");
                    setTimeout(location.reload.bind(location), 800);
                }

            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        // fungsi ajax load project 2
        var limit = 6;
        var start = 0;
        var action = 'inactive';
        function lazzy_loader3(limit) {
            var output = '';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            output += ' <div class="col-md-4">';
            output += ' <div class="post_data">';
            output += ' <p><span class="content-placeholder" style="width:100%; height: 270px;">&nbsp;</span></p>';
            output += '<p><span class="content-placeholder" style="width:100%; height: 50px;">&nbsp;</span></p>';
            output += '</div>';
            output += '</div>';
            $('#load_data_message3').html(output);
        }

        lazzy_loader3(limit);

        function load_data3(limit, start) {
            $.ajax({
                url: "<?= base_url('load-project-2'); ?>",
                method: "POST",
                data: {limit: limit, start: start},
                cache: false,
                success: function (data) {
                    if (data == '') {
                        $('#load_data_message3').html("");
                        action = 'active';
                    } else {
                        $('#load_data3').append(data);
                        $('#load_data_message3').html("");
                        action = 'inactive';
                    }
                }
            })
        }

        if (action == 'inactive') {
            action = 'active';
            load_data3(limit, start);
        }

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $("#load_data3").height() && action == 'inactive') {
                lazzy_loader3(limit);
                action = 'active';
                start = start + limit;
                setTimeout(function () {
                    load_data3(limit, start);
                }, 1000);
            }
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function () {

        // fungsi ajax load history project 
        var limit = 6;
        var start = 0;
        var action = 'inactive';

        function lazzy_loader2(limit) {
            var bindHTML = '';
            for (var counter = 0; counter < limit; counter++) {
                bindHTML += '<div class="timeline-item">';
                bindHTML += '<div class="animated-background">';
                bindHTML += '<div class="background-masker header-top"></div>';
                bindHTML += '<div class="background-masker header-left"></div>';
                bindHTML += '<div class="background-masker"></div>';
                bindHTML += '<div class="background-masker header-bottom"></div>';
                bindHTML += '<div class="background-masker subheader-left"></div>';
                bindHTML += '<div class="background-masker subheader-right"></div>';
                bindHTML += '<div class="background-masker subheader-bottom"></div>';
                bindHTML += '<div class="background-masker content-top"></div>';
                bindHTML += '</div>';
                bindHTML += '</div>';
            }
            $('#load_data_message2').html(bindHTML);
        }

        lazzy_loader2(limit);

        function load_data2(limit, start) {
            $.ajax({
                url: "<?= base_url('load-history-project'); ?>",
                method: "POST",
                data: {limit: limit, start: start},
                cache: false,
                success: function (data) {
                    if (data == '') {
                        $('#load_data_message2').html("");
                        action = 'active';
                    } else {
                        $('#load_data2').append(data);
                        $('#load_data_message2').html("");
                        action = 'inactive';
                    }
                }
            })
        }

        if (action == 'inactive') {
            action = 'active';
            load_data2(limit, start);
        }

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $("#load_data2").height() && action == 'inactive') {
                lazzy_loader2(limit);
                action = 'active';
                start = start + limit;
                setTimeout(function () {
                    load_data2(limit, start);
                }, 1000);
            }
        });
    });
</script>