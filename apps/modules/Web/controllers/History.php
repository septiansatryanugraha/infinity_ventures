<?php

class History extends AUTHWEB_Controller
{

    function __construct()
    {
        parent::__construct();
         $this->load->model('M_transaction');
    }

    function index()
    {
         $idUser = $this->session->userdata('id');
         $data = [
            'brand' => $this->M_transaction->getData(0, ['tbl_transaction.id_user' => $idUser]),
            'title' => 'My Investmen',
            'DetailOfProject' => $this->M_transaction->getData(0, ['tbl_transaction.id_user' => $idUser]),
        ];
        $this->load->view('v_history', $data);
    }


}
