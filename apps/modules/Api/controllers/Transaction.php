<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller
{
    const __tableName = 'tbl_transaction';
    const __tableId = 'id_transaction';
    const __model = 'M_transaction';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_project');
        $this->load->model('M_member');
        $this->load->model(self::__model);
    }

    public function getListTransaction()
    {
        $idUser = $this->session->userdata('id');
        $errCode = 0;
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $sessionKey = isset($post['session_key']) ? trim($post['session_key']) : null;
                $checkSession = $this->M_member->checkSession(['session_key' => $sessionKey]);
                if ($checkSession == null) {
                    $errCode++;
                    $errMessage = "Session Unauthenticated";
                } else {
                    $idUser = $checkSession->id_user;
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $errCode++;
                $errMessage = "User invalid.";
            }
        }
        if ($errCode == 0) {
            $getTransaction = $this->M_transaction->getData(0, ['tbl_transaction.id_user' => $idUser]);
            $result = array(
                'status' => 'True',
                'data' => $getTransaction,
            );
        } else {
            $result = array(
                'status' => 'false',
                'pesan' => 'My Investmen still empty / kasih gambar icon kosong jonnn'
            );
        }

        echo json_encode($result);
    }

    public function prosesBuy()
    {
        $username = $this->session->userdata('name');
        $idUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $idProject = trim($post['id_project']);
        $allocation = trim($post['allocation']);
        // $txid = trim($post['txid']);
        $allocation = (int) str_replace(',', '', $allocation);
        $fee = trim($post['fee']);
        $fee = (int) str_replace(',', '', $fee);

        $this->db->trans_begin();
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $sessionKey = isset($post['session_key']) ? trim($post['session_key']) : null;
                $checkSession = $this->M_member->checkSession(['session_key' => $sessionKey]);
                if ($checkSession == null) {
                    $errCode++;
                    $errMessage = "Session Unauthenticated";
                } else {
                    $idUser = $checkSession->id_user;
                    $username = $checkSession->name;
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $errCode++;
                $errMessage = "User invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idProject) == 0) {
                $errCode++;
                $errMessage = "Project is required.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_project->selectById($idProject);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = "Project invalid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status == 'CLOSED') {
                $errCode++;
                $errMessage = "Project has been closed.";
            }
        }
        if ($errCode == 0) {
            $checkTransaction = $this->M_transaction->selectByExist(['id_user' => $idUser, 'id_project' => $idProject]);
            if ($checkTransaction != null) {
                $errCode++;
                $errMessage = "Project is already bought.";
            }
        }
        if ($errCode == 0) {
            if (strlen($allocation) == 0) {
                $errCode++;
                $errMessage = "Allocation is required.";
            }
        }
        if ($errCode == 0) {
            if ($allocation < 0) {
                $errCode++;
                $errMessage = "Allocation should more than 0.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkValid->allocation_limit_member) {
                $errCode++;
                $errMessage = "Allocation should less than limit.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkValid->allocation_remain) {
                $errCode++;
                $errMessage = "Allocation remain " . $checkValid->allocation_remain . ".";
            }
        }
        if ($errCode == 0) {
            if (strlen($fee) == 0) {
                $errCode++;
                $errMessage = "Fee is required.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'id_user' => $idUser,
                    'id_project' => $idProject,
                    'allocation' => $allocation,
                    'fee' => $fee,
                    // 'txid' => $txid,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->insert(self::__tableName, $data);

                $dataProject = [
                    'allocation_remain' => $checkValid->allocation_remain - $allocation,
                ];
                $this->db->update('tbl_project', $dataProject, ['id_project' => $checkValid->id_project]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Project successfully apply'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesUpdate()
    {
        $username = $this->session->userdata('name');
        $idUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $id = trim($post['id_transaction']);
        $txid = trim($post['txid']);

        $this->db->trans_begin();
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $sessionKey = isset($post['session_key']) ? trim($post['session_key']) : null;
                $checkSession = $this->M_member->checkSession(['session_key' => $sessionKey]);
                if ($checkSession == null) {
                    $errCode++;
                    $errMessage = "Session Unauthenticated";
                } else {
                    $idUser = $checkSession->id_user;
                    $username = $checkSession->name;
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $errCode++;
                $errMessage = "User invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID Transaction invalid.";
            }
        }
        if ($errCode == 0) {
            $checkTransaction = $this->M_transaction->selectById($id);
            if ($checkTransaction == null) {
                $errCode++;
                $errMessage = "Transaction invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($txid) == 0) {
                $errCode++;
                $errMessage = "TIXID is required.";
            }
        }
        if ($errCode == 0) {
            if ($idUser != $checkTransaction->id_user) {
                $errCode++;
                $errMessage = "User is not valid with this transaction.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'txid' => $txid,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->update(self::__tableName, $data, ['id_transaction' => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Project successfully insert tix id'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
