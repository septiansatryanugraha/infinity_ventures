<?php

class M_homepage extends CI_Model
{

    function getAllProject()
    {
        $this->db->from('tbl_project');
        $this->db->where('status', 'OPEN');

        return $this->db->get()->result();
    }

    public function login($email, $pass)
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $data = $this->db->get();

        if ($data->num_rows() == 1) {
            $result = $data->row();
            $sessiondata['id'] = $result->id;
            $sessiondata['name'] = $result->name;
            $sessiondata['status'] = $result->status;
            $this->session->set_userdata($sessiondata);
            return $data->row();
        } else {
            return false;
        }
    }

    public function getUserById($id)
    {
        $sql = "SELECT * FROM tbl_user WHERE id  = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function LoadProject($limit, $start)
    {
        $this->db->select('tbl_project.id_project as id_project,		
			currency.code As code,
			tbl_project.name As name,
			tbl_project.allocation As allocation,
			tbl_project.allocation_limit_member As allocation_limit_member,
			tbl_project.allocation_remain as allocation_remain,
			tbl_project.price_token as price_token,
			tbl_project.description as description,
            tbl_project.deleted_date as deleted_date,
			tbl_project.image as image,
			tbl_project.detail as detail');
        $this->db->from('tbl_project');
        $this->db->join('currency', 'currency.id = tbl_project.id_currency', 'left');
        $this->db->where('tbl_project.deleted_date', NULL);
        $this->db->where('tbl_project.status', 'OPEN');
        $this->db->order_by("tbl_project.id_project", "DESC");
        $this->db->limit($limit, $start);

        return $this->db->get();
    }
}
