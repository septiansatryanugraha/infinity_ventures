<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends AUTH_Controller
{
    const __tableName = 'tbl_transaction';
    const __tableId = 'id_transaction';
    const __folder = 'v_transaction/';
    const __kode_menu = 'transaction';
    const __title = 'Transaction';
    const __model = 'M_transaction';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
        $this->load->model('M_balance');
        $this->load->model('M_project');
        $this->load->model('M_member');
        $this->load->model(self::__model);
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['project'] = $this->M_project->select();
            $data['member'] = $this->M_member->select();
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajaxList()
    {
        $idProject = $this->input->post('id_project');
        $idUser = $this->input->post('id_user');
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_project' => $idProject,
            'id_user' => $idUser,
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_transaction->getData(1, [], $filter);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->project;
            $row[] = $brand->member;
            $row[] = $brand->wallet;
            $row[] = $brand->allocation;
            $row[] = $brand->status;
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->created_date, 'by' => $brand->created_by]);
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->updated_date, 'by' => $brand->updated_by]);

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            $action .= "        <li><a href='#' class='detail-transaction' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_transaction . "'><i class='glyphicon glyphicon-info-sign'></i> Detail</a></li>";
            if ($accessEdit->menuview > 0 && $brand->status == 'PENDING') {
                $action .= "    <li><a href='" . base_url('edit-transaction') . "/" . $brand->id_transaction . "'><i class='fa fa-edit'></i> Edit</a></li>";
            }
            if ($accessEdit->menuview > 0 && $brand->status == 'PENDING') {
                $action .= "    <li><a href='" . base_url('confirm-transaction') . "/" . $brand->id_transaction . "'><i class='fa fa-check'></i> Confirm</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='delete-transaction' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_transaction . "'><i class='glyphicon glyphicon-trash'></i> Delete</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = "manual-transaction";
        $data['judul'] = "Manual " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            $data['project'] = $this->M_project->select();
            $data['member'] = $this->M_member->select();
            parent::loadkonten('' . self::__folder . 'add', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idUser = trim($this->input->post("id_user"));
        $idProject = trim($this->input->post("id_project"));
        $allocation = trim($this->input->post("allocation"));
        $allocation = (int) str_replace(',', '', $allocation);
        $fee = trim($this->input->post("fee"));
        $fee = (int) str_replace(',', '', $fee);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idUser) == 0) {
                $errCode++;
                $errMessage = "Member is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idProject) == 0) {
                $errCode++;
                $errMessage = "Project is required.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_project->selectById($idProject);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = "Project invalid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status == 'CLOSED') {
                $errCode++;
                $errMessage = "Project has been closed.";
            }
        }
        if ($errCode == 0) {
            $checkTransaction = $this->M_transaction->selectByExist(['id_user' => $idUser, 'id_project' => $idProject]);
            if ($checkTransaction != null) {
                $errCode++;
                $errMessage = "Project is already bought.";
            }
        }
        if ($errCode == 0) {
            if (strlen($allocation) == 0) {
                $errCode++;
                $errMessage = "Allocation is required.";
            }
        }
        if ($errCode == 0) {
            if ($allocation < 0) {
                $errCode++;
                $errMessage = "Allocation should more than 0.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkValid->allocation_limit_member) {
                $errCode++;
                $errMessage = "Allocation should less than limit.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkValid->allocation_remain) {
                $errCode++;
                $errMessage = "Allocation remain " . $checkValid->allocation_remain . ".";
            }
        }
        if ($errCode == 0) {
            if (strlen($fee) == 0) {
                $errCode++;
                $errMessage = "Fee is required.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'id_user' => $idUser,
                    'id_project' => $idProject,
                    'allocation' => $allocation,
                    'fee' => $fee,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->insert(self::__tableName, $data);

                $dataProject = [
                    'allocation_remain' => $checkValid->allocation_remain - $allocation,
                ];
                $this->db->update('tbl_project', $dataProject, ['id_project' => $checkValid->id_project]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Project successfully apply'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = "management-transaction";
        $data['judul'] = "Manual " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_transaction->selectById($id);
            if ($resultData != null && $resultData->status == 'PENDING') {
                $data['resultData'] = $resultData;
                $data['menuName'] = self::__kode_menu;
                $data['project'] = $this->M_project->select();
                $data['member'] = $this->M_member->select();
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('Transaction invalid.'); window.location = '" . base_url('management-transaction') . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idUser = trim($this->input->post("id_user"));
        $idProject = trim($this->input->post("id_project"));
        $allocation = trim($this->input->post("allocation"));
        $allocation = (int) str_replace(',', '', $allocation);
        $fee = trim($this->input->post("fee"));
        $fee = (int) str_replace(',', '', $fee);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID is required.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_transaction->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " invalid.";
            }
        }
        if ($errCode == 0) {
            $checkProjectValid = $this->M_project->selectById($checkValid->id_project);
            if ($checkProjectValid == null) {
                $errCode++;
                $errMessage = "Project invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($allocation) == 0) {
                $errCode++;
                $errMessage = "Allocation is required.";
            }
        }
        if ($errCode == 0) {
            if ($allocation < 0) {
                $errCode++;
                $errMessage = "Allocation should more than 0.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkProjectValid->allocation_limit_member) {
                $errCode++;
                $errMessage = "Allocation should less than limit.";
            }
        }
        if ($errCode == 0) {
            if ($allocation > $checkProjectValid->allocation_remain) {
                $errCode++;
                $errMessage = "Allocation remain " . $checkProjectValid->allocation_remain . ".";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'allocation' => $allocation,
                    'fee' => $fee,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->update(self::__tableName, $data, ['id_transaction' => $checkValid->id_transaction]);

                $dataProject = [
                    'allocation_remain' => ($checkProjectValid->allocation_remain + $checkValid->allocation) - $allocation,
                ];
                $this->db->update('tbl_project', $dataProject, ['id_project' => $checkValid->id_project]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Updated'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Detail($type = 0)
    {
        $post = array_merge($_POST, $_GET);
        $id = $post['id'];
        $resultData = $this->M_transaction->selectById($id);
        if ($resultData) {
            $checkProject = $this->M_project->selectById($resultData->id_project);
            $data['resultData'] = $resultData;
            $data['resultProject'] = $checkProject;

            echo '  <div class="modal fade" id="modal-transaction" role="dialog">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content">
                                ' . $this->load->view(self::__folder . 'detail', $data, TRUE) . '
                            </div>
                        </div>
					</div>';
        } else {
            echo "<script>alert('Transaction invalid.'); window.location = '" . base_url('management-transaction') . "';</script>";
        }
    }

    public function Confirm($id)
    {
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Confirm " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_transaction->selectById($id);
            if ($resultData != null) {
                if ($resultData->status == 'PENDING') {
                    $checkProject = $this->M_project->selectById($resultData->id_project);
                    $priceToken = $checkProject->price_token;
                    $tgePrecentage = $checkProject->tge_precentage;
                    $vestingPrecentage = $checkProject->vesting_precentage;
                    $amountToken = $resultData->allocation / $priceToken;

                    $data['resultData'] = $resultData;
                    $data['resultProject'] = $checkProject;
                    $data['amountToken'] = $amountToken;
                    $data['tge'] = $tgePrecentage * $amountToken;
                    $data['vesting'] = $vestingPrecentage * $amountToken;
                    $data['menuName'] = self::__kode_menu;
                    parent::loadkonten('' . self::__folder . 'confirm', $data);
                } else {
                    echo "<script>alert('Transaction has been confirmed.'); window.location = '" . base_url('management-transaction') . "';</script>";
                }
            } else {
                echo "<script>alert('Transaction unavailable.'); window.location = '" . base_url('management-transaction') . "';</script>";
            }
        }
    }

    public function prosesConfirm($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $status = trim($this->input->post("status"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_transaction->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " invalid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status != 'PENDING') {
                $errCode++;
                $errMessage = "This " . self::__title . " is " . $checkValid->status . ".";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status is required.";
            }
        }
        if ($errCode == 0) {
            try {
                $checkProject = $this->M_project->selectById($checkValid->id_project);
                $priceToken = $checkProject->price_token;
                $tgePrecentage = $checkProject->tge_precentage;
                $vestingPrecentage = $checkProject->vesting_precentage;
                $amountToken = $checkValid->allocation / $priceToken;

                $data = [
                    'status' => $status,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                if ($status == 'APPROVE') {
                    $data = array_merge($data, [
                        'amount_token' => $amountToken,
                        'tge' => $tgePrecentage * $amountToken,
                        'vesting' => $vestingPrecentage * $amountToken,
                    ]);
                    $this->M_balance->insert(self::__tableName, [self::__tableId => $id], 'in', 'TRANSACTION PROJECT', 'Transaction bid');
                } else {
                    $getProject = $this->M_project->selectById($checkValid->id_project);
                    $dataProject = [
                        'allocation_remain' => $getProject->allocation_remain + $checkValid->allocation,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    ];
                    $this->db->update('tbl_project', $dataProject, ['id_project' => $getProject->id_project]);
                }
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Confirmed'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $id = $post['id'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID is required.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_transaction->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " invalid.";
            }
        }
        if ($errCode == 0) {
            $checkProjectValid = $this->M_project->selectById($checkValid->id_project);
            if ($checkProjectValid == null) {
                $errCode++;
                $errMessage = "Project invalid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);

                $dataProject = [
                    'allocation_remain' => $checkProjectValid->allocation_remain + $checkValid->allocation,
                ];
                $this->db->update('tbl_project', $dataProject, ['id_project' => $checkValid->id_project]);

                if ($checkValid->status == 'APPROVE') {
                    $this->db->update('tbl_project_history', ['deleted_date' => date('Y-m-d H:i:s')], ['ref_table' => self::__tableName, 'ref_id' => $id]);
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Deleted'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
