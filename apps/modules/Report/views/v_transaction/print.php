<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=" . $filename . ".xls");

?>
<style>
    .text-right {
        text-align: right;
    }
    .text-center {
        text-align: center;
    }
    .valign-middle {
        vertical-align: middle;
    }
</style>
<table>
    <tr><td class="text-center" colspan="11">REPORT TRANSACTION</td></tr>
    <tr><td colspan="11">&nbsp;</td></tr>
    <tr>
        <td colspan="2">Transaction Date</td>
        <td colspan="9">: <?= ($filter['all_date'] > 0) ? 'ALL DATE' : date('d-m-Y', strtotime($filter['tanggal_awal'])) . ' - ' . date('d-m-Y', strtotime($filter['tanggal_akhir'])) ?></td>
    </tr>
    <tr>
        <td colspan="2">Project</td>
        <td colspan="9">: <?= $checkProject->name ?></td>
    </tr>
    <tr>
        <td colspan="2">Category</td>
        <td colspan="9">: <?= $checkProject->category ?></td>
    </tr>
    <tr>
        <td colspan="2">Currency</td>
        <td colspan="9">: <?= $checkProject->currency ?></td>
    </tr>
    <tr>
        <td colspan="2">Total Allocation</td>
        <td colspan="9">: <?= number_format($checkProject->allocation, 0, ".", ",") ?></td>
    </tr>
    <tr>
        <td colspan="2">Price Token</td>
        <td colspan="9">: <?= number_format($checkProject->price_token, 3, ".", ",") ?></td>
    </tr>
    <tr>
        <td colspan="2">TGE</td>
        <td colspan="9">: <?= $checkProject->tge_precentage * 100; ?> % <?= (strlen($checkProject->clift_amount) > 0) ? 'and Clift ' . $checkProject->clift_amount . ' ' . $checkProject->clift : '' ?></td>
    </tr>
    <tr>
        <td colspan="2">Vesting</td>
        <td colspan="9">: <?= $checkProject->vesting_precentage * 100 . ' % for ' . number_format($checkProject->vesting_amount, 0, ".", ",") . ' ' . $checkProject->vesting ?></td>
    </tr>
</table>
<table border="1" id="table-data">
    <thead>
        <tr>
            <td class="text-center valign-middle">#</td>
            <td class="text-center valign-middle">Member</td>
            <td class="text-center valign-middle">Allocation ( <?= $checkProject->currency ?> )</td>
            <td class="text-center valign-middle">Price Token</td>
            <td class="text-center valign-middle">Amount Token</td>
            <td class="text-center valign-middle">TGE ( <?= $checkProject->tge_precentage * 100; ?> % )</td>
            <td class="text-center valign-middle">Fee</td>
            <td class="text-center valign-middle">Distribution</td>
            <td class="text-center valign-middle">Vesting ( <?= $checkProject->vesting_precentage * 100; ?> % )</td>
            <td class="text-center valign-middle">TXID</td>
            <td class="text-center valign-middle">Wallet Non Exchange</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($resultData as $key => $value) { ?>
            <tr>
                <td><?= $key + 1; ?></td>
                <td><?= $value->member; ?></td>
                <td class="text-right"><?= number_format($value->allocation, 0, ".", ","); ?></td>
                <td class="text-right"><?= number_format($checkProject->price_token, 3, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->amount_token, 3, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->tge, 3, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->fee, 0, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->distribution, 3, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->vesting, 3, ".", ","); ?></td>
                <td><?= $value->txid; ?></td>
                <td><?= $value->wallet; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>