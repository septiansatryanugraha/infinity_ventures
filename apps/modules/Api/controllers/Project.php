<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller
{
    const __tableName = 'tbl_project';
    const __tableId = 'id_project';
    const __model = 'M_project';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_project');
        $this->load->model(self::__model);
    }

    public function getListProject()
    {
        $getProject = $this->M_project->getData(0, ['status' => 'OPEN']);
        if ($getProject > 0) {
            $result = [
                'status' => 'True',
                'data' => $getProject,
            ];
        } else {
            $result = [
                'status' => 'false',
                'pesan' => 'Project still empty / kasih gambar icon kosong jonnn'
            ];
        }

        echo json_encode($result);
    }
}
