<?php

class Homepage extends AUTHWEB_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_homepage');
        $this->load->model('M_transaction');
        $this->load->model('M_project');
    }

    function index()
    {
        $data = [
            'all' => $this->M_homepage->getAllProject(),
            'title' => 'Data Project',
            'DetailOfProject' => $this->M_project->getData(0, ['status' => 'OPEN']),
        ];
        $this->load->view('v_homepage', $data);
    }

    function RegisterUser()
    {
        $data = [
            'title' => 'Register Member',
        ];
        $this->load->view('v_register', $data);
    }

    function ProfilUser()
    {
        $IdUser = $this->session->userdata('id');

        $data = [
            'title' => 'Register Member',
            'result' => $this->M_homepage->getUserById($IdUser),
        ];
        $this->load->view('v_profil', $data);
    }

    function CekApplyProject()
    {
        if ($this->session->userdata('is_login') != TRUE) {
            $out = ['status' => false, 'pesan' => 'Maaf harus login dahulu'];
        } else {
            $out = ['status' => true, 'pesan' => ' Berhasil Checkout'];
        }
        echo json_encode($out);
    }

    public function LoadProject()
    {
        $output = '';
        $data = $this->M_homepage->LoadProject($this->input->post('limit'), $this->input->post('start'));
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $row) {
                $getProject = $this->M_project->selectById($row->id_project);
                $createdDate = date('Y-m-d', strtotime($getProject->created_date . '+3 days'));
                $labelNew = '';
                if (date('Y-m-d') <= $createdDate) {
                    $labelNew .= '  <div class="ribbon ribbon-top-left">
                                        <span>baru</span>
                                    </div>';
                }
                $output .= '<div class="col-md-4">
                                ' . $labelNew . '
                                <div class="thumbnail">
                                    <img class="gambar-project" src="' . base_url() . $row->image . '">
                                    <div class="caption">
                                        <h4><center>' . $row->name . '</center></h4>
                                        <div class="row">
                                            <div class="caption" style="margin-bottom: 10px; margin-top: 10px;">
                                                <h4><center>' . $row->code . ' ' . number_format($row->allocation_remain, 0, ".", ",") . '</center></h4>
                                            </div>
                                            <div class="col-md-5">
                                                <input type="hidden" name="quantity" id="' . $row->id_project . '" value="1" class="quantity form-control">
                                            </div>
                                        </div>
                                        <button class="add_cart btn btn-success btn-block add-project1" data-id=' . "'" . $brand->id_project . "'" . '>Apply</button></a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>
                            </div>';
            }
        }

        echo $output;
    }

    public function LoadProject2()
    {
        $output = '';
        $data = $this->M_homepage->LoadProject($this->input->post('limit'), $this->input->post('start'));
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $row) {
                $button = '<button class="btn btn-success btn-block" disabled>Bought</button>';
                $checkTransaction = $this->M_transaction->selectByExist(['id_user' => $this->session->userdata('id'), 'id_project' => $row->id_project]);
                if ($checkTransaction == null) {
                    $button = '<a data-toggle="modal" data-target="#exampleModal' . $row->id_project . '"><button class="add_cart btn btn-success btn-block">Apply</button></a>';
                }
                $getProject = $this->M_project->selectById($row->id_project);
                $createdDate = date('Y-m-d', strtotime($getProject->created_date . '+7 days'));
                $labelNew = '';
                if (date('Y-m-d') <= $createdDate) {
                    $labelNew .= '  <div class="ribbon ribbon-top-left">
                                        <span>baru</span>
                                    </div>';
                }
                $output .= '<div class="col-md-4">
                                ' . $labelNew . '
                                <div class="thumbnail">
                                    <img class="gambar-project" src="' . base_url() . $row->image . '">
                                    <div class="caption">
                                        <h4><center>' . $row->name . '</center></h4>
                                            <div class="row">
                                                <div class="caption" style="margin-bottom: 10px; margin-top: 10px;">
                                                    <h4><center>' . $row->code . ' ' . number_format($row->allocation_remain, 0, ".", ",") . '</center></h4>
                                                </div>
                                                <div class="col-md-5">
                                                    <input type="hidden" name="quantity" id="' . $row->id_project . '" value="1" class="quantity form-control">
                                                </div>
                                            </div>
                                            ' . $button . '
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>';
            }
        }

        echo $output;
    }

    public function LoadHistoryProject()
    {
        $output = '';
        $data = $this->M_transaction->loadData($this->input->post('limit'), $this->input->post('start'), ['id_user' => $this->session->userdata('id')]);
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $row) {
                $getProject = $this->M_project->selectById($row->id_project);
                $output .= '<div class="recent_post">
                                <ul>
                                    <li>
                                        <a href="#"><img src="' . base_url() . $getProject->image . '" />
                                            <h3>' . $getProject->name . '</h3>
                                            <span class="date"><b>' . $row->status . '</b></span>
                                            <p>' . date('d-m-Y H:i:s', strtotime($row->created_date)) . '</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>';
            }
        }

        echo $output;
    }

    public function loginUser()
    {
        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == TRUE) {
                $email = trim($_POST['email']);
                $password = trim(md5($_POST['password']));
                $data = $this->M_homepage->login($email, $password);
                $session['id'] = $data->id;
                $session['name'] = $data->name;
                $session['status'] = $data->status;
                $session['is_login'] = TRUE;
                $stat = $data->status;
                if ($stat == 1) {
                    $this->session->set_userdata($session);
                    $status = 1;
                    $URL_home = base_url('home-dashboard');
                    $json['status'] = true;
                    $json['url_home'] = $URL_home;
                    $json['pesan'] = "Login Success.";
                } else {
                    $json['pesan'] = "Sorry your account does not active yet.";
                }

                if ($data == false) {
                    $json['pesan'] = "Email or Password invalid.";
                } else {
                    $this->session->set_userdata($session);
                }
            } else {
                $json['pesan'] = "Cannot Login, Please check your Email and Password.";
            }
        } else {
            redirect('home-dashboard');
        }

        echo json_encode($json);
    }

    public function logoutUser()
    {
        $this->session->sess_destroy();
        redirect('home-dashboard');
    }
}
