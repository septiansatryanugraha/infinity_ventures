<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends CI_Model
{
    const __tableName = 'tbl_user';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByExist($arrWhere = [], $id = null)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if ($id != null) {
            $sql .= " AND " . self::__tableId . " <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qTransaction = "SELECT * FROM tbl_transaction WHERE deleted_date IS NULL AND id_user = '{$id}'";
            $resTransaction = $this->db->query($qTransaction)->row();
            if ($resTransaction != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    public function select()
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->where('status', 1);
        $this->db->order_by(self::__tableName . '.name', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }

    public function login($data)
    {
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->where('email', $data['email']);
        $this->db->where('password', $data['password']);
        $this->db->where('status', 1);
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkSession($data)
    {
        $this->db->select('api_session.*, ' . self::__tableName . '.name');
        $this->db->select(self::__tableName . '.email, ' . self::__tableName . '.name');
        $this->db->from('api_session');
        $this->db->join(self::__tableName, self::__tableName . '.id = api_session.id_user', 'left');
        $this->db->where('api_session.status > 0');
        $this->db->limit(1);

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
}
