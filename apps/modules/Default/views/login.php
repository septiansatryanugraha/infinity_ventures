<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login | Infinity Ventures</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/style.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/fonts/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/fonts/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/style-login.css">
        <link rel="icon" href="<?= base_url() ?>/assets/dist/img/logo-icon.png">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/iCheck/square/blue.css">
        <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
        <link rel="stylesheet" href="<?= base_url(); ?>assets/toastr/toastr.css">
        <?= $script_captcha; // javascript recaptcha ?>
    </head>
    <style>
        .login-page { background: url(<?= base_url(); ?>assets/dist/img/login-admin.jpg);background-position: center;background-repeat: no-repeat; background-size: cover; }
        .field-icon {
            float: left;
            font-size: 17px;
            position: relative;
            z-index: 2;
            color: #555;
        }
        a {
            color: #555;
        }
        /*[ login more ]*/
        .login100-more {
            width: calc(100% - 560px);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            position: relative;
            z-index: 1;
        }
        .login100-more::before {
            content: "";
            display: block;
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            /* background: rgba(0,0,0,0.1);*/
        }
        @media (max-width: 992px) {
            .login100-more {
                width: 50%;
            }
        }
        @media (max-width: 768px) {
            .login100-more {
                display: none;
            }
        }
        .wrap .wrap2 input,
        .wrap .ex {
            display: inline-block;
        }
        .wrap {
            position: relative;
            width: 100%;
            height: 20px;
            margin-bottom: 30px;
        }
        .ex3 {
            position: absolute;
            right: 0;
            top: 10%;
            height: 20px;
            width: 20px;
            margin-right: 30px;
            /*opacity: 0;*/
            font-size: 20px;
            line-height: 30px;
            border: 1px solid transparent;
            text-align: center;
            cursor: pointer;
            transition: all 0.8s;
            display: none;
        }
        .wrap2 .ex {
            display: inline-block;
        }
        .wrap2 {
            position: relative;
            width: 100%;
            height: 20px;
        }
        .ex2 {
            position: absolute;
            right: 0;
            top: 10%;
            height: 20px;
            width: 20px;
            z-index: 99;
            /*opacity: 0;*/
            margin-right: 5px;
            font-size: 20px;
            line-height: 30px;
            border: 1px solid transparent;
            text-align: center;
            cursor: pointer;
            display: none;
        }
    </style>
    <body class="hold-transition login-page">
        <div class="login-box" style="margin-top: 120px;">
            <div class="login-logo">
                <img src="<?= base_url(); ?>assets/dist/img/logo.png" height="80px"> 
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><font style='color: white; font-size: 13px;'> <b>Login Admin </b></font></p>
                <form action="<?= base_url('Default/Auth/login'); ?>" method="post" id="FormLogin">
                    <div class="form-group has-feedback">
                        <div class="wrap">
                            <input type="text" class="form-control signup-input" id="username" placeholder="Username" name="username"><div id="bantuan1" class="ex3">&times;</div></div>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="input-group wrap2" id="show_hide_password">
                        <div class="wrap2">
                            <input class="form-control signup-input2" name="password" type="password" id="password-field" placeholder="Password"><div id="bantuan2" class="ex2">&times;</div></div>
                        <div class="input-group-addon">
                            <a href=""><i class="fa fa-eye field-icon" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <br>
                    <?= $captcha ?>
                    <br>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" id="remember"><font style='color: white; font-size: 13px;'> <b> Remember Me </b></font>
                                </label>
                            </div>
                        </div>
                        <div id='btn_loading'></div>
                        <div id="hilang">
                            <div class="col-xs-4">
                                <button type="submit" id="btnSignUp" class="btn btn-primary btn-block btn-flat klik"><i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login</button>
                            </div>
                        </div>
                        <div id="buka">
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat klik"><i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div> 
        </div>
        <script type="text/javascript" >
            function formValidation(oEvent) {
                oEvent = oEvent || window.event;
                var txtField = oEvent.target || oEvent.srcElement;
                var t1ck = true;
                var t1ck_user = true;
                var t1ck_pass = true;
                var msg = " ";
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

                // if(document.getElementById("username").value.match(mailformat) )
                // { 
                //   t1ck=true; msg = msg + "Benar";
                // } else {
                //   t1ck=false; msg = msg + "Email tidak valid";
                // } 

                if (document.getElementById("username").value.length < 4) {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (document.getElementById("username").value.length < 1) {
                    t1ck_user = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (document.getElementById("password-field").value.length < 2) {
                    t1ck = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                if (document.getElementById("password-field").value.length < 1) {
                    t1ck_pass = false;
                    msg = msg + "Your name should be minimun 3 char length";
                }
                // alert(msg + t1ck);
                if (t1ck) {
                    document.getElementById("btnSignUp").disabled = false;
                } else {
                    document.getElementById("btnSignUp").disabled = true;
                }
                if (t1ck_user) {
                    document.getElementById("bantuan1").style.display = "block";
                } else {
                    document.getElementById("bantuan1").style.display = "none";
                }
                if (t1ck_pass) {
                    document.getElementById("bantuan2").style.display = "block";
                } else {
                    document.getElementById("bantuan2").style.display = "none";
                }
            }

            function resetForm() {
                document.getElementById("btnSignUp").disabled = true;
                var frmMain = document.forms[0];
                frmMain.reset();
            }

            window.onload = function () {
                var btnSignUp = document.getElementById("btnSignUp");
                var username = document.getElementById("username");
                var password = document.getElementById("password-field");

                var t1ck = false;
                document.getElementById("btnSignUp").disabled = true;
                username.onkeyup = formValidation;
                password.onkeyup = formValidation;
            }

            $(document).ready(function () {
                $("#show_hide_password a").on('click', function (event) {
                    event.preventDefault();
                    if ($('#show_hide_password input').attr("type") == "text") {
                        $('#show_hide_password input').attr('type', 'password');
                        $('#show_hide_password i').addClass("fa-eye-slash");
                        $('#show_hide_password i').removeClass("fa-eye-slash");
                    } else if ($('#show_hide_password input').attr("type") == "password") {
                        $('#show_hide_password input').attr('type', 'text');
                        $('#show_hide_password i').removeClass("fa-eye-slash");
                        $('#show_hide_password i').addClass("fa-eye-slash");
                    }
                });
            });

            $(function () {
                $("#buka").hide();
                //------------------------Proses Login Ajax-------------------------//
                $('#FormLogin').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        beforeSend: function () {
                            $("#buka").hide();
                            $("#hilang").hide();
                            $("#btn_loading").html("<div class='col-xs-4'><button type='submit' class='btn btn-primary btn-block btn-flat klik' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
                            $("#btn_loading").show();
                        },
                        url: $(this).attr('action'),
                        type: "POST",
                        cache: false,
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (json) {
                            if (json.status == true) {
                                $("#btn_loading").hide();
                                $("#buka").show();
                                toastr.success(json.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                                    "closeButton": true});
                                window.location = json.url_home;
                            } else {
                                $("#btn_loading").hide();
                                $("#hilang").show();
                                toastr.error(json.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                                    "closeButton": true});
                            }
                        }
                    });
                });
            });

            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.ex3').click(function () {
                    $('.signup-input').val("");
                    document.getElementById("bantuan1").style.display = "none";
                    document.getElementById("btnSignUp").disabled = true;
                });
            });

            $(document).ready(function () {
                $('.ex2').click(function () {
                    $('.signup-input2').val("");
                    document.getElementById("bantuan2").style.display = "none";
                    document.getElementById("btnSignUp").disabled = true;
                });
            });
        </script>
        <!-- /.login-box -->
        <!-- jQuery 2.2.3 -->
        <script src="<?= base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>
        <script src="<?= base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
    </body>
</html>