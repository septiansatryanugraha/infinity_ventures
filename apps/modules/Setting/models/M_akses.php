
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_akses extends CI_Model
{

    public function selectById($id)
    {
        $sql = "SELECT * FROM grup WHERE deleted_date IS NULL AND grup_id = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    // VIEW USER PRIVILEGE
    public function hakAkses($id)
    {
        if (!$id)
            return FALSE;
        $query = "  select a.id_menuakses, b.id_menu as menus, a.id_menu, b.nama_menu, a.grup_id, b.menuparent, c.nama_grup, a.view, a.add, a.edit, a.del, b.menu_file
					from (select * from menu_akses where grup_id = '" . $id . "') a
					right join (select a.*, b.nama_menu as menuparent from tbl_menu a left join tbl_menu b on a.parent = b.id_menu WHERE a.deleted_date IS NULL AND b.deleted_date IS NULL) b
					on a.id_menu = b.id_menu
					left join grup c
					on a.grup_id = c.grup_id
					order by menuparent ASC";
        $data = $this->db->query($query);

        return $data->result();
    }

    function save($data)
    {
        $result = $this->db->insert('menu_akses', $data);

        return $result;
    }
}
