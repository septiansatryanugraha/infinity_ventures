<div class="col-md-12 well">
    <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;">
        <i class="fa fa-location-arrow"></i>Detail Transaction
    </h3>
    <div class="col-sm-12">
        <div class="nav-tabs-custom" id="newContain">
            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Member </label>
                        <div class="col-sm-9"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->member . ' ( ' . $resultData->email . ' )'; ?>" readOnly></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Wallet Non Exchange </label>
                        <div class="col-sm-9"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->wallet; ?>" readOnly></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Project </label>
                        <div class="col-sm-9"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->project; ?>" readOnly></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Allocation ( <?= $resultProject->currency; ?> ) </label>
                        <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultData->allocation, 0, ".", ","); ?>" readOnly></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Price Token </label>
                        <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultProject->price_token, 3, ".", ","); ?>" readOnly></div>
                    </div>
                    <?php if ($resultData->status == 'APPROVE') { ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Amount Token </label>
                            <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultData->amount_token, 3, ".", ","); ?>" readOnly></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">TGE ( <?= $resultProject->tge_precentage * 100; ?> % ) </label>
                            <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultData->tge, 3, ".", ","); ?>" readOnly></div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Fee </label>
                        <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultData->fee, 0, ".", ","); ?>" readOnly></div>
                    </div>
                    <?php if ($resultData->status == 'APPROVE') { ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Vesting ( <?= $resultProject->vesting_precentage * 100; ?> % ) </label>
                            <div class="col-sm-5"><input type="text" class="form-control text-right" style="background: #FFF;" value="<?= number_format($resultData->vesting, 3, ".", ","); ?>" readOnly></div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">TXID </label>
                        <div class="col-sm-9"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->txid; ?>" readOnly></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status </label>
                        <div class="col-sm-3"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->status; ?>" readOnly></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button class="btn btn-danger close-modal" data-dismiss="modal"> Close</button>
        </div>
    </div>
</div>