<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AUTH_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->userdata = $this->session->userdata('userdata');
        $this->session->set_flashdata('segment', explode('/', $this->uri->uri_string()));
        $_SESSION['sideweb'] = "backend";

        if ($this->session->userdata('status') == '') {
            redirect('login-admin');
        }
    }

    public function loadkonten($page, $data)
    {
        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function updateProfil()
    {
        if ($this->userdata != '') {
            $data = $this->M_admin->select($this->userdata->id);
            $this->session->set_userdata('userdata', $data);
            $this->userdata = $this->session->userdata('userdata');
        }
    }

    public static function rotate($source_array, $keep_keys = TRUE)
    {
        $new_array = array();
        foreach ($source_array as $key => $value) {
            $value = ($keep_keys === TRUE) ? $value : array_values($value);
            foreach ($value as $k => $v) {
                $new_array[$k][$key] = $v;
            }
        }

        return $new_array;
    }

    public static function loadCreatedUpdatedContent($arrCreatedUpdated = array())
    {
        $content = "";
        $datetime = $arrCreatedUpdated['datetime'];
        $by = $arrCreatedUpdated['by'];
        if (strlen($datetime) > 0 && strlen($by) > 0) {
            if (strlen($datetime) > 0) {
                $datetime = date('d-m-Y H:i:s', strtotime($datetime));
            }

            $content = "<table>
                            <tr>
                                <td style='width: 45px; vertical-align: top;'><b>Date</b></td>
                                <td style='vertical-align: top;'>:</td>
                                <td>" . $datetime . "</td>
                            </tr>
                            <tr>
                                <td><b>By</b></td>
                                <td>:</td>
                                <td>" . $arrCreatedUpdated['by'] . "</td>
                            </tr>
                        </table>";
        }

        return $content;
    }

    public static function createFolder($folderName = "")
    {
        if (strlen($folderName) > 0) {
            $path = "./upload/";
            if (!is_dir($path)) {
                mkdir($path);
            }
            $path = $path . "/" . $folderName . "/";
            if (!is_dir($path)) {
                mkdir($path);
            }
        }

        return $folderName;
    }
}

/* End of file MY_Auth.php */
/* Location: ./application/core/MY_Auth.php */