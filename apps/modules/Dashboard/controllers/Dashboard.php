<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
    }

    public function index()
    {
        $data['member'] = $this->M_total->total_member();
        $data['project'] = $this->M_total->total_project();
        $data['transaction'] = $this->M_total->total_transaction();
        $data['userdata'] = $this->userdata;

        $data['page'] = "home";
        $data['judul'] = "Dashboard";
        parent::loadkonten('home', $data);
    }

    public function changeYear()
    {
        $tahun = $this->input->post('tahun');
        $_SESSION['tahun'] = $tahun;
        echo json_encode($_SESSION);
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */