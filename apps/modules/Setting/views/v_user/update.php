<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <div class="row">
                <div class="col-md-10">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Admin Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="nama" id="nama" name="nama" aria-describedby="sizing-addon2" value="<?= $resultData->nama; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="email" placeholder="email" name="email" aria-describedby="sizing-addon2" value="<?= $resultData->email; ?>">
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-2">
                                <select name="status" class="form-control select-status"  aria-describedby="sizing-addon2">
                                    <?php foreach ($dataStatus as $status) { ?>
                                        <option value="<?= $status->id_status; ?>" <?= ($status->id_status == $resultData->status) ? 'selected' : ''; ?>>
                                            <?= $status->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-6">
                                <label class="form-control"><?= $resultData->username; ?></label>
                            </div>
                        </div>	
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="password-field" placeholder="password"  name="password" aria-describedby="sizing-addon2"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
                                </span>
                                <p style='color: red; font-size: 14px;'><b> *fill in the password if you want to updated</b></p>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Grup</label>
                            <div class="col-sm-3">
                                <select name="grup_id" id="grup" class="form-control select-group" aria-describedby="sizing-addon2">
                                    <?php foreach ($datagrup as $data) { ?>
                                        <option value="<?= $data->grup_id; ?>" <?= ($data->grup_id == $resultData->grup_id) ? 'selected' : ''; ?>>
                                            <?= $data->nama_grup; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div id="buka"> 
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            <a class="klik ajaxify" href="<?= base_url('user'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                        </div>
                        <div id="btn_loading">
                            <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </form>
    </div>
    <!-- /.row -->
</section>

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url('update-user') . '/' . $resultData->id; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?= base_url("user"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    // untuk select2 original
    $(function () {
        $(".select-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".select-group").select2({
            placeholder: " -- Pilih Group -- "
        });
    });

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>	