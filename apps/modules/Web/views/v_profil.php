<?php
include('header.php');

?>

<style type="text/css">
    .field-icon {
        float: left;
        margin-left: 90%;
        margin-top: -35px;
        font-size: 17px;
        position: relative;
        z-index: 999;
        color: #ccc;
    }
</style>

<div class="container"><br/>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-user"></i>
                    <h3>Data Profile</h3>
                </div>
                <div class="widget-content">
                    <form action="" method="POST" id="form-update">
                        <input type="hidden" class="form-control" name="id" value="<?= $result->id ?>">
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Email</label>
                            <div class="controls">
                                <input type="text" class="form-control" value="<?= $result->email ?>" disabled>
                            </div>        
                        </div> 
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Name Member</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $result->name ?>">
                            </div>        
                        </div>    
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Phone</label>
                            <div class="controls">
                                <input type="text" class="form-control" id="phone" name="phone" value="<?= $result->phone ?>" onkeypress="return hanyaAngka(event)" /> 
                            </div>  
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Wallet Non Exchange</label>
                            <div class="controls">
                                <input type="text" class="form-control" id="wallet" name="wallet" value="<?= $result->wallet ?>" /> 
                            </div>  
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Password</label>
                            <div class="controls">
                                <input class="form-control signup-input2" name="password" type="password" id="password-field" placeholder="Password">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> 
                            </div>
                        </div>
                        <div class="form-actions">
                            <div id='btn_loading'></div>
                            <div id="hilang">
                                <button type="submit" id="btnSignUp" class="btn btn-primary">Update</button> 
                                <button class="btn">Cancel</button>
                            </div>
                        </div> <!-- /form-actions -->
                    </form>
                </div> 
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<?php include('footer.php'); ?>

<script type="text/javascript">
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });


    $('#form-update').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();
        if (error == 0) {
            var name = $("#name").val();
            var name = name.trim();
            if (name.length == 0) {
                error++;
                message = "Name member cannot be empty .";
            }
        }
        if (error == 0) {
            var phone = $("#phone").val();
            var phone = phone.trim();
            if (phone.length == 0) {
                error++;
                message = "Phone cannot be empty.";
            }
        }
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#hilang").hide();
                    $("#btn_loading").html("<button class='btn btn-primary' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
                    $("#btn_loading").show();
                },
                url: '<?= base_url('api-update-member'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#btn_loading").hide();
                    $("#hilang").show();
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    $("#btn_loading").hide();
                    $("#hilang").show();
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>