<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_balance extends CI_Model
{
    const __tableName = 'tbl_project_history';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($refTable, $arrWhere = [], $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";
        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }
        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }
        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }
        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }
        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }
        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }
        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }
        //Insert History Item
        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = [];
                $data['id_project'] = $getRef['id_project'];
                $data['id_user'] = $getRef['id_user'];
                $data['ref_table'] = $refTable;
                $data['ref_id'] = $refId;
                $data['date'] = $date;
                $data['module'] = $module;
                $data['description'] = $description;
                $dataItem = [];
                if ($type == 'in') {
                    $data['allocation_in'] = $getRef['allocation'];
                    $data['allocation_out'] = 0;
                } else if ($type == 'out') {
                    $data['allocation_in'] = 0;
                    $data['allocation_out'] = $getRef['allocation'];
                }
                $data['created_date'] = $getRef['created_date'];
                $data['created_by'] = $getRef['created_by'];
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyItem = $this->db->insert(self::__tableName, $data);
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }
        $result = [
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        ];

        return $result;
    }

    public function update($refTable, $arrWhere = [], $type, $module, $description)
    {
        $errCode = 0;
        $errMessage = "";
        if ($errCode == 0) {
            if (strlen($refTable) == 0) {
                $errCode++;
                $errMessage = "Ref Table not declared.";
            }
        }
        if ($errCode == 0) {
            if (count($arrWhere) == 0) {
                $errCode++;
                $errMessage = "Arr Where not declared..";
            }
        }
        if ($errCode == 0) {
            if (strlen($type) == 0) {
                $errCode++;
                $errMessage = "Type not declared..";
            }
        }
        if ($errCode == 0) {
            if ($type != 'in' && $type != 'out') {
                $errCode++;
                $errMessage = "Type must 'in' or 'out'.";
            }
        }
        if ($errCode == 0) {
            if (strlen($module) == 0) {
                $errCode++;
                $errMessage = "Modules not declared..";
            }
        }
        if ($errCode == 0) {
            $getRef = $this->db->get_where($refTable, $arrWhere)->row_array();
            if ($getRef == null) {
                $errCode++;
                $errMessage = "Data invalid.";
            }
        }
        if ($errCode == 0) {
            $indexId = str_replace('tbl_', 'id_', $refTable);
            $refId = $getRef[$indexId];
            if (strlen($refId) == 0) {
                $errCode++;
                $errMessage = "Reff ID not declared.";
            }
        }

        if ($errCode == 0) {
            $q = "  SELECT * FROM " . self::__tableName . "
                    WHERE deleted_date IS NULL
                    AND ref_table = '{$refTable}'
                    AND ref_id = '{$refId}'";
            $getLastHistory = $this->db->query($q)->row_array();
            if ($getLastHistory == NULL) {
                $errCode++;
                $errMessage = "History invalid.";
            }
        }

        if ($errCode == 0) {
            try {
                $date = $getRef['date'];
                if (strlen($date) == 0) {
                    $date = date('Y-m-d');
                }
                $data = [];
                $data['date'] = $date;
                $data['description'] = $description;
                $dataItem = [];
                if ($type == 'in') {
                    $data['allocation_in'] = $getRef['allocation'];
                    $data['allocation_out'] = 0;
                } else if ($type == 'out') {
                    $data['allocation_in'] = 0;
                    $data['allocation_out'] = $getRef['allocation'];
                }
                $data['updated_date'] = $getRef['updated_date'];
                $data['updated_by'] = $getRef['updated_by'];
                $historyFinance = $this->db->update(self::__tableName, $data, [self::__tableId => $getLastHistory[self::__tableId]]);
            } catch (Exception $e) {
                $errCode++;
                $errMessage = $e->getMessage();
            }
        }
        $result = [
            'errCode' => $errCode,
            'errMessage' => $errMessage,
        ];

        return $result;
    }
}
