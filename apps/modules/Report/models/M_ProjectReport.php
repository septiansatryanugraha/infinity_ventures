<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ProjectReport extends CI_Model
{
    const __tableName = 'tbl_project';
    const __tableId = 'id_project';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $arrWhere = [], $filter = [])
    {
        $status = isset($filter['status']) ? $filter['status'] : null;
        $tanggalAwal = isset($filter['tanggal_awal']) ? $filter['tanggal_awal'] : null;
        $tanggalAkhir = isset($filter['tanggal_akhir']) ? $filter['tanggal_akhir'] : null;
        $allDate = isset($filter['all_date']) ? $filter['all_date'] : null;

        $sql = "SELECT " . self::__tableName . ".*, 
                CONCAT(category.code,' - ',category.name) AS category, 
                currency.code AS currency
                FROM " . self::__tableName . "
                LEFT JOIN category ON category.id = " . self::__tableName . ".id_category
                LEFT JOIN currency ON currency.id = " . self::__tableName . ".id_currency
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND " . self::__tableName . ".status = '{$status}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".updated_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".updated_date <= '{$tanggalAkhir}'";
            }
        }
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }
}
