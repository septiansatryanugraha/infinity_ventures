<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionReport extends AUTH_Controller
{
    const __tableName = 'tbl_transaction';
    const __tableId = 'id_transaction';
    const __folder = 'v_transaction/';
    const __kode_menu = 'report-transaction';
    const __title = 'Report Transaction';
    const __model = 'M_TransactionReport';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
        $this->load->model('M_project');
        $this->load->model('M_member');
        $this->load->model('M_transaction');
        $this->load->model(self::__model);
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['project'] = $this->M_project->select();
            $data['member'] = $this->M_member->select();
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajaxList()
    {
        $idProject = $this->input->post('id_project');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'id_project' => $idProject,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $list = [];
        $checkProject = [];
        if (strlen($idProject) > 0) {
            $checkProject = $this->M_project->selectById($idProject);
            $list = $this->M_TransactionReport->getData(1, [], $filter);
        }

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->member;
            $row[] = '<div style="text-align: right;">' . number_format($brand->allocation, 0, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($checkProject->price_token, 3, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->amount_token, 3, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->tge, 3, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->fee, 0, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->distribution, 3, ".", ",") . '</div>';
            $row[] = '<div style="text-align: right;">' . number_format($brand->vesting, 3, ".", ",") . '</div>';
            $row[] = $brand->txid;
            $row[] = $brand->wallet;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function printReport()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak " . self::__title;
        if ($access->menuview == 0) {
            echo "<script>alert('You don't have access.'); window.close();</script>";
        } else {
            $idProject = $this->input->post('id_project');
            $tanggalAwal = $this->input->post('tanggal_awal');
            $tanggalAkhir = $this->input->post('tanggal_akhir');
            $allDate = $this->input->post('all_date');

            $filter = array(
                'id_project' => $idProject,
                'tanggal_awal' => $tanggalAwal,
                'tanggal_akhir' => $tanggalAkhir,
                'all_date' => isset($allDate) ? 1 : 0,
            );

            $resultData = [];
            $checkProject = [];
            if (strlen($idProject) > 0) {
                $checkProject = $this->M_project->selectById($idProject);
                $resultData = $this->M_TransactionReport->getData(1, [], $filter);
            }

            if (!empty($resultData)) {
                $data['filter'] = $filter;
                $data['resultData'] = $resultData;
                $data['checkProject'] = $checkProject;
                $data['filename'] = 'report_transaction_' . date('YmdHis');

                $this->load->view(self::__folder . '/print', $data);
            } else {
                echo "<script>alert('Transaction empty.'); window.close();</script>";
            }
        }
    }
}
