<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="box-header">
                <div class="form-group ">
                    <div class="col-md-4" style="margin-left: 0px; margin-bottom: 10px;">
                        <?php if ($accessAdd > 0) { ?>
                            <a class="klik ajaxify" href="<?= base_url('manual-transaction'); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Manual</button></a>
                        <?php } ?>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="search-form" style="">
                    <div class="form-group ">
                        <label class="control-label">Filter</label>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Project</label>
                        <div class="col-sm-3">
                            <select name="id_project" class="form-control select-project" id="id_project">
                                <option value="all">All Project</option>
                                <?php foreach ($project as $data) { ?>
                                    <option value="<?= $data->id_project; ?>">
                                        <?= $data->name; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Member</label>
                        <div class="col-sm-3">
                            <select name="id_user" class="form-control select-user" id="id_user">
                                <option value="all">All Member</option>
                                <?php foreach ($member as $data) { ?>
                                    <option value="<?= $data->id; ?>">
                                        <?= $data->name . ' ( ' . $data->email . ' )'; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-3">
                            <select name="status" id="status" class="form-control select-status" aria-describedby="sizing-addon2">
                                <option value="all">All Status</option>
                                <option value="PENDING">PENDING</option>
                                <option value="APPROVE">APPROVE</option>
                                <option value="REJECT">REJECT</option>
                            </select>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Last Update Date</label>
                        <div class="col-sm-6">
                            <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block; background: #FFF;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                            <span>s/d</span>
                            <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block; background: #FFF;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">  
                        <label class="col-sm-2 control-label"></label>       
                        <div class="col-sm-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="all_date" name="all_date"/>All Date
                            </label>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="box-footer">
                        <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Show</button>
                    </div>
                    <div class="box-footer"><br></div>
                </div>
            </div>
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Project</th>
                                <th>Member</th>
                                <th>Wallet Non Exchange</th>
                                <th>Allocation</th>
                                <th>Status</th>
                                <th style="width: 200px;">Created</th>
                                <th style="width: 200px;">Updated</th>
                                <th style="width: 45px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="modal-place"></div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        // untuk datepicker
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });
        $(".select-project").select2({
            placeholder: " -- Choose Project -- "
        });
        $(".select-user").select2({
            placeholder: " -- Choose Member -- "
        });
        $(".select-status").select2({
            placeholder: " -- Choose Status -- "
        });
    });

    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var id_project = $("#id_project").val();
        var id_user = $("#id_user").val();
        var status = $("#status").val();
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[10, 50, 75, 100, 150, -1], [10, 50, 75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 10,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?= base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sInfoPostFix": "",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('ajax-transaction') ?>",
                "type": "POST",
                data: {id_project: id_project, id_user: id_user, status: status, tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".detail-transaction", function () {
        var id = $(this).attr("data-id");
        $.ajax({
            method: "POST",
            url: "<?= base_url('detail-transaction'); ?>",
            data: "id=" + id
        }).done(function (data) {
            $('#modal-place').html(data);
            $('#modal-transaction').modal('show');
        })
    })

    $(document).on("click", ".close-modal", function () {
        $('.modal-backdrop.fade.in').remove();
        $('body>#modal-transaction').remove();
        $('body #modal-transaction').removeClass('in');
        $('body #modal-transaction').style('display', 'none');
    })

    $(document).on("click", ".delete-transaction", function () {
        var id = $(this).attr("data-id");
        swal({
            title: "Delete Data?",
            text: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Delete",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?= base_url('delete-transaction'); ?>",
                data: "id=" + id,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + id + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        hapus_berhasil();
                        setTimeout("window.location='<?= base_url("management-transaction"); ?>'", 450);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    reload_table();
                }
            });
        });
    });
</script>