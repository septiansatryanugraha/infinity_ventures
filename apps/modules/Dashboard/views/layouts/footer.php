</div>
<!-- /.content-wrapper -->
<!-- footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Dashboard Admin
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?= date("Y") ?> Infinity Ventures</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<!-- js -->
<!-- REQUIRED JS SCRIPTS -->
<!-- FastClick -->
<script src="<?= base_url(); ?>admin-lte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>admin-lte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>admin-lte/dist/js/demo.js"></script>
<script src="<?= base_url(); ?>admin-lte/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url(); ?>admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url(); ?>admin-lte/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url(); ?>admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- checkbox -->
<script src="<?= base_url(); ?>admin-lte/plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>admin-lte/dist/js/app.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/bootstrap-summernote/summernote.min.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/js/ajax.js"></script>
<script src="<?= base_url(); ?>assets/toastr/toastr.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/icon-picker/fontawesome-iconpicker.js"></script>

<div id="loadingAjak"></div>
<script type="text/javascript">
    $('.sidebar-menu a').click(function () {
        // Remove all class active and display none
        $(".sidebar-menu .active").removeClass("active");
        $(".sidebar-menu .treeview-menu.menu-open").css("display", "none");
        $(".sidebar-menu .treeview-menu.menu-open").removeClass("menu-open");

        // add class active and set display block
        $('.' + this.getAttribute("parent")).addClass('active');
        $('.' + this.getAttribute("child")).addClass('active');
        $('.' + this.getAttribute("child")).closest("ul.treeview-menu").addClass('menu-open');
        $('.' + this.getAttribute("child")).closest("ul.treeview-menu").css("display", "block");
    });

    //klik loading ajax
    $(document).ready(function () {
        $(document).on('keypress', '.number_only', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.number_only').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            }
        });
        $('.number_only').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val(0);
            }
        });
        $(document).on('keypress', '.number_decimal', function (event) {
//            if ((event.which != 44 && event.which < 48 || event.which > 57)) {
            if ((event.which != 46 && event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.number_decimal').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            }
        });
        $('.number_decimal').on('focusout', function () {
            var x = $(this).val();
            var x_comma = x.split('.').length;
            if (x == 0 || x.length == 0 || x_comma > 2) {
                $(this).val(0);
            }
        });
        //----- Accounting Format -----// 
        $('.formatCurrency').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            } else {
                $(this).val(accounting.unformat(x));
            }
        });
        $('.formatCurrency').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val(0);
            } else {
                $(this).val(accounting.formatMoney(x));
            }
        });
        $('.formatCurrencyDecimal').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            } else {
                $(this).val(accounting.unformat(x));
            }
        });
        $('.formatCurrencyDecimal').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("0");
            } else {
                $(this).val(accounting.formatNumber(x, 3));
            }
        });
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='<?= base_url(); ?>assets/dist/img/loading.gif' height='130'> ");
            $('#loadingAjak').show();
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $('#loadingAjak').hide();
                }
            });
            return true;
        });
    });
    // handle ajax link dengan konten
    var base_url = '<?= base_url(); ?>';
    var ajaxify = [null, null, null];

    $('.content-wrapper').on('click', '.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });

    $('.sidebar-menu').on('click', ' li > a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });

    $('.pull-left').on('click', 'a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    // loading ajax
    $.ajaxSetup({
        beforeSend: function (xhr) {
            $("#loading2").show().html("<img src='<?= base_url() ?>assets/dist/img/loaders.gif' height='130'> ");
            $('#loadingAjak').show();
        },
        complete: function () {
            $("#loading2").hide();
            $('#loadingAjak').hide();
        },
        error: function () {
            $("#loading2").hide();
            $('#loadingAjak').hide();
        }
    });
    // load konten ajax
    var function_ajaxify = function (e, ele) {
        e.preventDefault();
        var url = $(ele).attr("href");
        //var pageContent = $('.page-content');
        var pageContentBody = $('.content-wrapper');
        if (url != ajaxify[2]) {
            ajaxify.push(url);
            history.pushState(null, null, url);
        }
        ajaxify = ajaxify.slice(-3, 5);
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: {status_link: 'ajax'},
            dataType: "html",
            success: function (res) {
                if (res.search("content-login") > 0) {
                    window.location = base_url + 'login';
                } else {
                    //hide_loading_bar();
                    pageContentBody.html(res);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: 'error/error_404',
                    data: {url: ajaxify[1], url1: ajaxify[2]},
                    dataType: "html",
                    success: function (res) {
                        if (res == 'out') {
                            window.location = base_url + 'login';
                        } else {
                            //hide_loading_bar();
                            pageContentBody.html(res);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //hide_loading_bar();
                    }
                });
            }
        });
    }

    function checkLoadImage(identifier) {
        var fileInput = document.getElementById(identifier);
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            toastr.error('Image format .jpeg/.jpg/.png/.gif only.', 'Warning', {timeOut: 5000}, toastr.options = {"closeButton": true});
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '" width="150" height="auto"/>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
        // validasi size file
        var size = document.getElementById(identifier);
        if (size.files[0].size > 1007200) {
            toastr.error('Maximum file 1 MB', 'Warning', {timeOut: 5000}, toastr.options = {"closeButton": true});
            size.value = '';
            return false;
        }
    }
</script> 
</html>