<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akses extends AUTH_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_akses', 'akses');
        $this->load->model('M_sidebar');
    }

    // ADD DATA
    public function hakAkses($id)
    {
        /* ini harus ada boss */
        $data['page'] = "Hak Akses";
        $data['judul'] = "Hak Akses";
        $access = $this->M_sidebar->access('edit', 'grup');
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            if ($id > 1) {
                $this->_set_rules_privilege();
                $data['privilege'] = $this->akses->hakAkses($id);
                $data['groupname'] = $this->akses->selectById($id);
                $data['grup_id'] = $id;
                $data['userdata'] = $this->userdata;
                parent::loadkonten('v_akses/v_hak_akses', $data);
            } else {
                echo "<script>alert('Super Administrator cannot changed.'); window.location = '" . base_url('user-grup') . "';</script>";
            }
        }
    }

    public function updateHakAkses($id)
    {
        $this->db->trans_begin();
        try {
            $menu = $this->input->post('id_menu');

            $dataGrup = [
                'updated_date' => date('Y-m-d H:i:s'),
                'updated_by' => $this->session->userdata('username'),
            ];
            $result = $this->db->update('grup', $dataGrup, ['grup_id' => $id]);

            // reset auto increment
            $this->db->delete('menu_akses', ['grup_id' => $id]);
            $this->db->query("ALTER TABLE menu_akses DROP id_menuakses");
            $this->db->query("ALTER TABLE menu_akses ADD id_menuakses INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");
            foreach ($menu as $row => $idMenu) {
                $view = isset($_REQUEST['view'][$idMenu]);
                $add = isset($_REQUEST['add'][$idMenu]);
                $edit = isset($_REQUEST['edit'][$idMenu]);
                $del = isset($_REQUEST['del'][$idMenu]);

                $data = [
                    'id_menu' => $idMenu,
                    'grup_id' => $id,
                    'view' => $view,
                    'add' => $add,
                    'edit' => $edit,
                    'del' => $del,
                ];
                $this->db->insert('menu_akses', $data);
            }
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Hak akses berhasil di update'];
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => 'Hak akses gagal disimpan ! ' . $ex->getMessage()];
        }

        echo json_encode($out);
    }

    function _set_rules_privilege()
    {
        $this->form_validation->set_rules('id_menu', 'Menu', '');
    }
}
