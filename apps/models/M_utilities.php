<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_utilities extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getCategory()
    {
        $this->db->from('category');
        $data = $this->db->get();

        return $data->result();
    }

    public function getCurrency()
    {
        $this->db->from('currency');
        $data = $this->db->get();

        return $data->result();
    }
}
