<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=" . $filename . ".xls");

?>
<style>
    .text-right {
        text-align: right;
    }
    .text-center {
        text-align: center;
    }
    .valign-middle {
        vertical-align: middle;
    }
</style>
<table>
    <tr><td class="text-center" colspan="11">REPORT PROJECT</td></tr>
    <tr><td colspan="11">&nbsp;</td></tr>
    <tr>
        <td colspan="2">Created Date</td>
        <td colspan="9">: <?= ($filter['all_date'] > 0) ? 'ALL DATE' : date('d-m-Y', strtotime($filter['tanggal_awal'])) . ' - ' . date('d-m-Y', strtotime($filter['tanggal_akhir'])) ?></td>
    </tr>
</table>
<table border="1" id="table-data">
    <thead>
        <tr>
            <td class="text-center" style="vertical-align: middle;">#</td>
            <td class="text-center" style="vertical-align: middle;">Category</td>
            <td class="text-center" style="vertical-align: middle;">Name</td>
            <td class="text-center" style="vertical-align: middle;">Currency</td>
            <td class="text-center" style="vertical-align: middle;">Allocation</td>
            <td class="text-center" style="vertical-align: middle;">Limit</td>
            <td class="text-center" style="vertical-align: middle;">Remain</td>
            <td class="text-center" style="vertical-align: middle;">Price Token</td>
            <td class="text-center" style="vertical-align: middle;">TGE</td>
            <td class="text-center" style="vertical-align: middle;">Vesting</td>
            <td class="text-center" style="vertical-align: middle;">Status</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($resultData as $key => $value) {
            $vestingPrecentage = $value->vesting_precentage * 100;
            $expVestingPrecentage = explode('.', $vestingPrecentage);
            if (isset($expVestingPrecentage[1])) {
                $vestingPrecentage = number_format($vestingPrecentage, 1, ".", ",");
            }

            ?>
            <tr>
                <td><?= $key + 1; ?></td>
                <td><?= $value->category; ?></td>
                <td><?= $value->name; ?></td>
                <td><?= $value->currency; ?></td>
                <td class="text-right"><?= number_format($value->allocation, 0, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->allocation_limit_member, 0, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->allocation_remain, 0, ".", ","); ?></td>
                <td class="text-right"><?= number_format($value->price_token, 3, ".", ","); ?></td>
                <td><?= $value->tge_precentage * 100; ?> % <?= (strlen($value->clift_amount) > 0) ? 'and Clift ' . $value->clift_amount . ' ' . $value->clift : '' ?></td>
                <td><?= $vestingPrecentage . ' % for ' . $value->vesting_amount . ' ' . $value->vesting ?></td>
                <td><?= $value->status; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>