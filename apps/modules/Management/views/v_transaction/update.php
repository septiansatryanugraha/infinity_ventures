<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-update" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Member</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" aria-describedby="sizing-addon2" value="<?= $resultData->member . ' (' . $resultData->email . ' )'; ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Project</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" aria-describedby="sizing-addon2" value="<?= $resultData->project; ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Allocation</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control text-right number_only formatCurrency" name="allocation" placeholder="Allocation" id="allocation" aria-describedby="sizing-addon2" value="<?= number_format($resultData->allocation, 0, ".", ",") ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Fee</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control text-right number_only formatCurrency" name="fee" placeholder="Fee" id="fee" aria-describedby="sizing-addon2" value="<?= number_format($resultData->fee, 0, ".", ",") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="save" id="save" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Save</button>
                                <a class="klik ajaxify" href="<?= base_url('management-transaction'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="save" id="save" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Save Data?",
            text: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Save",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url("update-transaction") . '/' . $resultData->id_transaction; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $("#buka").show();
                $("#btn_loading").hide();
                if (result.status == true) {
                    setTimeout("window.location='<?= base_url($page); ?>'", 450);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    return false;
                }
            })
        });
    });
</script>