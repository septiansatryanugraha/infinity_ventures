 
<style>
    #btn_loading {
        display: none;
    }
</style>

<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-confirm" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Member </label>
                                <div class="col-sm-5"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->member . ' ( ' . $resultData->email . ' )'; ?>" readOnly></div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Project </label>
                                <div class="col-sm-5"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->project; ?>" readOnly></div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Allocation ( <?= $resultProject->currency; ?> )</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($resultData->allocation, 0, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Price Token</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($resultProject->price_token, 3, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Amount Token</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($amountToken, 3, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">TGE ( <?= $resultProject->tge_precentage * 100; ?> % )</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($tge, 3, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Fee</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($resultData->fee, 0, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Vesting ( <?= $resultProject->vesting_precentage * 100; ?> % )</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control text-right" style="background: #FFF;" placeholder="TGE" aria-describedby="sizing-addon2" value="<?= number_format($vesting, 3, ".", ","); ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">TXID </label>
                                <div class="col-sm-5"><input type="text" class="form-control" style="background: #FFF;" value="<?= $resultData->txid; ?>" readOnly></div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select name="status" class="form-control select-status" id="status">
                                        <option></option>
                                        <option value="APPROVE">APPROVE</option>
                                        <option value="REJECT">REJECT</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Confirm</button>
                                <a class="klik ajaxify" href="<?= base_url('management-transaction'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Confirm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    $('#form-confirm').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Confirm Transaction?",
            text: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Confirm",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url("process-confirm-transaction") . '/' . $resultData->id_transaction; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $("#buka").show();
                $("#btn_loading").hide();
                if (result.status == true) {
                    setTimeout("window.location='<?= base_url("management-transaction"); ?>'", 450);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    return false;
                }
            })
        });
    });

    $(function () {
        $(".select-status").select2({
            placeholder: " -- Choose Status -- "
        });
    });
</script>