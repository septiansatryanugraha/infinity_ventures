<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
        $this->load->model('M_admin');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;

        $data['page'] = "profile";
        $data['judul'] = "Profile";
        $data['datagrup'] = $this->M_admin->select_group();
        $data['dataStatus'] = $this->M_admin->select_status();
        parent::loadkonten('v_profil/profile', $data);
    }

    public function update()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('last_update_by', 'last_update_by', 'trim|required');
        $id = $this->input->post('id');

        $nama = $this->input->post('nama');
        $foto = $this->input->post('foto');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($grupId) == 0) {
                $errCode++;
                $errMessage = "Grup wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Format Email salah.";
            }
        }
        if ($errCode == 0) {
            if (strlen($username) == 0) {
                $errCode++;
                $errMessage = "Username wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkUsername = $this->M_user->selectByUsername($username);
            if ($checkUsername != null) {
                $errCode++;
                $errMessage = "Username sudah digunakan.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Password wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'grup_id' => $grupId,
                    'nama' => $nama,
                    'email' => $email,
                    'username' => $username,
                    'password' => md5($password),
                    'status' => 7,
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by' => $this->session->userdata('username'),
                    'updated_date' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->session->userdata('username'),
                ];
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Saved'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        if ($this->form_validation->run() == TRUE) {
            $data['nama'] = $nama;

            $config['upload_path'] = './upload/user/';
            $config['allowed_types'] = 'jpg|png';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                $error = ['error' => $this->upload->display_errors()];
            } else {
                $data_foto = $this->upload->data();
                $data['foto'] = $data_foto['file_name'];
                $this->session->set_userdata($data);
            }

            $result = $this->M_admin->update($data, $id);
            if ($result > 0) {
                $this->session->set_userdata('nama', $nama);
                $out = ['status' => true, 'pesan' => ' Profile Successfully Updated'];
            } else {
                $out = ['status' => false, 'pesan' => 'Profile fail to Updated !'];
            }
        } else {
            $out = ['status' => false, 'pesan' => 'Profile fail to Updated !'];
        }

        echo json_encode($out);
    }

    public function ubahPassword()
    {
        $this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
        $this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
        $this->form_validation->set_rules('last_update_by', 'last_update_by', 'trim|required');
        $password = $this->input->post('passLama');

        $id = $this->session->userdata('id');
        if ($this->form_validation->run() == TRUE) {
            if (md5($this->input->post('passLama')) == $this->session->userdata('password')) {
                if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
                    $out = ['status' => false, 'pesan' => 'Password Baru dan Konfirmasi Password harus sama !'];
                } else {
                    $data = [
                        'password' => md5($this->input->post('passBaru'))
                    ];

                    $result = $this->M_admin->update($data, $id);
                    if ($result > 0) {
                        $this->session->set_userdata('password', $password);
                        $this->updateProfil();
                        $out = ['status' => true, 'pesan' => ' Password berhasil di update'];
                    } else {
                        $out = ['status' => false, 'pesan' => 'Maaf gagal update password !'];
                    }
                }
            } else {
                $out = ['status' => false, 'pesan' => 'Maaf gagal update password !'];
            }
        } else {
            $out = ['status' => false, 'pesan' => 'Maaf gagal update password kosong !'];
        }
        echo json_encode($out);
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */