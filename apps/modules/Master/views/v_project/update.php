<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-update" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Category</label>
                                <div class="col-sm-3">
                                    <?php if (!$checkForeign) { ?>
                                        <select name="id_category" class="form-control select-category" id="id_category">
                                            <option></option>
                                            <?php foreach ($category as $data) { ?>
                                                <option value="<?= $data->id; ?>" <?= ($data->id == $resultData->id_category) ? "selected" : ""; ?>>
                                                    <?= $data->code . ' - ' . $data->name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" aria-describedby="sizing-addon2" value="<?= $resultData->category ?>" readOnly>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Currency</label>
                                <div class="col-sm-3">
                                    <?php if (!$checkForeign) { ?>
                                        <select name="id_currency" class="form-control select-currency" id="currency">
                                            <option></option>
                                            <?php foreach ($currency as $data) { ?>
                                                <option value="<?= $data->id; ?>" <?= ($data->id == $resultData->id_currency) ? "selected" : ""; ?>>
                                                    <?= $data->code . ' - ' . $data->name; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" aria-describedby="sizing-addon2" value="<?= $resultData->currency ?>" readOnly>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name" placeholder="Project Name" id="name" aria-describedby="sizing-addon2" value="<?= $resultData->name ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Allocation</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-right number_only formatCurrency" name="allocation" placeholder="Project Allocation" id="allocation" aria-describedby="sizing-addon2" value="<?= number_format($resultData->allocation, 0, ".", ",") ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Allocation Limit Member</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-right number_only formatCurrency" name="allocation_limit_member" placeholder="Project Allocation Limit Member" id="allocation_limit_member" aria-describedby="sizing-addon2" value="<?= number_format($resultData->allocation_limit_member, 0, ".", ",") ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Price Token</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-right formatCurrencyDecimal" name="price_token" placeholder="Price Token" id="price_token" aria-describedby="sizing-addon2" value="<?= number_format($resultData->price_token, 3, ".", ",") ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">TGE ( % )</label>
                                <div class="col-sm-1">
                                    <input type="text" class="form-control text-right formatDecimal processVesting" name="tge_precentage" placeholder="TGE" id="tge_precentage" aria-describedby="sizing-addon2" value="<?= (is_numeric($resultData->tge_precentage)) ? $resultData->tge_precentage * 100 : $resultData->tge_precentage; ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label">Vesting</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-right formatNumber processVesting" style="width: 20%; display: inline-block;" name="vesting_amount" placeholder="Amount" id="vesting_amount" aria-describedby="sizing-addon2" value="<?= number_format($resultData->vesting_amount, 0, ".", ",") ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                    <span>&nbsp;</span>
                                    <?php if (!$checkForeign) { ?>
                                        <select name="vesting" class="form-control select-vesting" id="vesting" style="width: 40%; display: inline-block;">
                                            <option></option>
                                            <option value="DAY" <?= ($resultData->vesting == 'DAY') ? "selected" : ""; ?>>DAY</option>
                                            <option value="MONTH" <?= ($resultData->vesting == 'MONTH') ? "selected" : ""; ?>>MONTH</option>
                                            <option value="YEAR" <?= ($resultData->vesting == 'YEAR') ? "selected" : ""; ?>>YEAR</option>
                                        </select>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" style="width: 40%; display: inline-block;" aria-describedby="sizing-addon2" value="<?= $resultData->vesting ?>" readOnly>
                                    <?php } ?>
                                    <span>&nbsp;</span>
                                    <input type="text" class="form-control text-right" style="width: 20%; display: inline-block; background: <?= (!$checkForeign) ? "#FFF" : "#eee" ?>;" id="vesting_precentage" name="vesting_precentage" aria-describedby="sizing-addon2" readOnly>
                                    <span>%</span>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-2 control-label">Clift</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control text-right number_only" style="width: 20%; display: inline-block;" name="clift_amount" placeholder="Amount" id="clift_amount" aria-describedby="sizing-addon2" value="<?= number_format($resultData->clift_amount, 0, ".", ",") ?>" <?= (!$checkForeign) ? "" : "readOnly" ?>>
                                    <span>&nbsp;</span>
                                    <?php if (!$checkForeign) { ?>
                                        <select name="clift" class="form-control select-clift" id="clift" style="width: 40%; display: inline-block;">
                                            <option></option>
                                            <option value="DAY" <?= ($resultData->clift == 'DAY') ? "selected" : ""; ?>>DAY</option>
                                            <option value="MONTH" <?= ($resultData->clift == 'MONTH') ? "selected" : ""; ?>>MONTH</option>
                                            <option value="YEAR" <?= ($resultData->clift == 'YEAR') ? "selected" : ""; ?>>YEAR</option>
                                        </select>
                                    <?php } else { ?>
                                        <input type="text" class="form-control" style="width: 40%; display: inline-block;" aria-describedby="sizing-addon2" value="<?= $resultData->clift ?>" readOnly>
                                    <?php } ?>
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-1"></div>
                                <div id="slider">
                                    <img class="img-thumbnail" src="<?= base_url() . $image; ?>" alt="your image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputFoto" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-4">
                                    <input type="file" class="form-control" name="project_image" id="project_image" onchange="return checkLoadImage('project_image')"/>
                                </div>
                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">max 1 Mb </small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-4">
                                    <textarea class="form-control" name="description" id="description" aria-describedby="sizing-addon2" rows="5"><?= $resultData->description ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="update" id="update" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                                <a class="klik ajaxify" href="<?= base_url('master-project'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="update" id="update" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Update Data?",
            text: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Update",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url("update-project") . '/' . $resultData->id_project; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $("#buka").show();
                $("#btn_loading").hide();
                if (result.status == true) {
                    setTimeout("window.location='<?= base_url("master-project"); ?>'", 450);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    return false;
                }
            })
        });
    });

    $(function () {
        $(document).on('keypress', '.formatNumber', function (event) {
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.formatNumber').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            }
        });
        $('.formatNumber').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val(0);
            }
        });
        $(document).on('keypress', '.formatDecimal', function (event) {
            if ((event.which != 46 && event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.formatDecimal').on('focusin', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("");
            } else {
                $(this).val(accounting.unformat(x));
            }
        });
        $('.formatDecimal').on('focusout', function () {
            var x = $(this).val();
            if (x == 0 || x.length == 0) {
                $(this).val("0");
            } else {
                $(this).val(accounting.formatNumber(x, 1));
            }
        });
        $('#tge_precentage').on('focusout', function () {
            var x = $(this).val();
            if (x > 100) {
                $(this).val("0");
            }
        });
        $('#tge_precentage').on('keyup', function () {
            var x = $(this).val();
            if (x > 100) {
                $(this).val("0");
            }
        });
        processVesting();
        $('.processVesting').on('focusout', function () {
            processVesting();
        });
        $('.processVesting').on('keyup', function () {
            processVesting();
        });
        $(".select-category").select2({
            placeholder: " -- Choose Category -- "
        });
        $(".select-currency").select2({
            placeholder: " -- Choose Currency -- "
        });
        $(".select-clift").select2({
            placeholder: " -- Choose Type -- "
        });
        $(".select-vesting").select2({
            placeholder: " -- Choose Type -- "
        });
    });

    function processVesting() {
        var tge_precentage = $('#tge_precentage').val();
        var vesting_amount = $('#vesting_amount').val();
        if ((tge_precentage >= 0 && tge_precentage.length > 0 && tge_precentage <= 100) && (vesting_amount >= 0 && vesting_amount.length > 0)) {
            var remain = 100 - tge_precentage;
            var vesting_precentage = 0;
            if (vesting_amount == 0) {
                vesting_precentage = remain;
            } else {
                vesting_precentage = remain / vesting_amount;
            }
            $('#vesting_precentage').val(vesting_precentage);
            console.log(tge_precentage);
        } else {
            $('#vesting_precentage').val("");
        }
    }
</script>