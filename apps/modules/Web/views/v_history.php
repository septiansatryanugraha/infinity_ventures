<?php
include('header.php');

?>

<style type="text/css">
    .gambar-project{
        width: 250px;
        height: 250px !important;
        object-fit: cover;
    }
</style>

<div class="container"><br/>
    <div class="col-md-12">
        <div class="widget ">
            <div class="widget-content">
                <?php foreach ($brand as $row) : ?>
                    <div class="col-md-3">
                        <div class="ribbon ribbon-top-left">
                            <span><?php echo $row->status; ?></span>
                        </div>
                        <div class="thumbnail">
                            <img class="gambar-project" src="<?php echo base_url() . $row->image; ?>">
                            <div class="caption">
                                <h4><center><?php echo $row->project; ?></center></h4>
                                <div class="row">
                                    <div class="" style="margin-left: 20px;margin-bottom: 10px; margin-top: 10px;">
                                        <h4><center><?php echo $row->currency; ?> <?php echo $row->allocation; ?></center></h4>
                                    </div>
                                </div>
                                <a data-toggle="modal" data-target="#exampleModal<?php echo $row->id_transaction ?>">
                                    <button class="add_cart btn btn-success btn-block"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Detail</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>


                <?php
                $no = 0;
                foreach ($DetailOfProject as $row): $no++;

                    ?>

                    <div class="modal fade" id="exampleModal<?= $row->id_transaction; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="container" id="newContain<?= $row->id_transaction; ?>">
                                    <form  method="POST">
                                        <input type="hidden" name="id_transaction" value="<?= $row->id_transaction ?>">
                                        <div class="modal-body scroll">
                                            <div class="row" style="overflow-y : auto;">
                                                <div class="col-sm-2">
                                                    <img class="thumbnail" src="<?= base_url($row->image) ?>" style="width:180px;">
                                                </div>
                                                <div class="col-sm-4">
                                                    <p><?php echo $row->description ?></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-md-1">                     
                                                    <label class="control-label" for="firstname">Round ID</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->project ?></p>
                                                    </div>        
                                                </div>
                                                <div class="control-group col-md-2">                     
                                                    <label class="control-label" for="firstname">Max Allocation</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->allocation ?></p>
                                                    </div>        
                                                </div> 
                                                <div class="control-group col-md-1">                     
                                                    <label class="control-label" for="firstname">TGE</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->tge ?> %</p>
                                                    </div>        
                                                </div>
                                                <div class="control-group col-md-2">                     
                                                    <label class="control-label" for="firstname">Liniear Vesting</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->vesting ?> %</p>
                                                    </div>        
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="control-group col-md-1">                     
                                                    <label class="control-label" for="firstname">State</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->status ?></p>
                                                    </div>        
                                                </div>
                                                <div class="control-group col-md-2">                     
                                                    <label class="control-label" for="firstname">Transaction Date</label>
                                                    <div class="controls">
                                                        <p class="static"><?php echo $row->created_date ?></p>
                                                    </div>        
                                                </div>
                                                <div class="control-group col-md-3">                     
                                                    <label class="control-label" for="firstname">TXID BSCSCAN.COM</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="txid" value="<?php echo $row->txid ?>">
                                                    </div>        
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary simpan" value="<?= $row->id_transaction; ?>"><i class=" fa fa-paper-plane"></i> Apply</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class=" fa fa-remove"></i> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</div>

<?php include('footer.php'); ?>


<script type="text/javascript">
    // fungsi ajax add bid project
    $(".simpan").click(function () {
        var error = 0;
        var message = "";
        var data = $("#newContain" + $(this).val() + ">form").serialize();
//        var data = $("#exampleModal" + $(this).val() + ">form").serialize();
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    swal({
                        imageUrl: "<?= base_url(); ?>assets/web/images/ajax-loader.gif",
                        title: "Please wait..",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                url: '<?= base_url('api-transaction-update-project'); ?>',
                type: "POST",
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    setTimeout(location.reload.bind(location), 1000);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    swal("Warning", result.pesan, "warning");
                }
            })
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>