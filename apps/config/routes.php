<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

/*   WEB PAGE  */

$route['login-user'] = 'Web/Homepage/loginUser';
$route['logout-user'] = 'Web/Homepage/logoutUser';

$route['home-dashboard'] = 'Web/Homepage/index';
$route['home-dashboard-history'] = 'Web/History/index';
$route['home-dashboard-profil'] = 'Web/Homepage/ProfilUser';

$route['load-project'] = 'Web/Homepage/LoadProject';
$route['load-project-2'] = 'Web/Homepage/LoadProject2';
$route['load-history-project'] = 'Web/Homepage/LoadHistoryProject';
$route['register-member'] = 'Web/Homepage/RegisterUser';
$route['cek-apply-project'] = 'Web/Homepage/CekApplyProject';
/* End of WEB PAGE */


/*   API SERVICES  */
$route['api-registration-member'] = "Api/Member/prosesRegistration";

$route['api-login-member'] = "Api/Member/prosesLogin";
$route['api-logout-member'] = "Api/Member/prosesLogout";
$route['api-check-session'] = "Api/Member/checkSession";

$route['api-update-member'] = "Api/Member/prosesUpdateMember";
$route['api-transaction-buy-project'] = "Api/Transaction/prosesBuy";
$route['api-transaction-update-project'] = 'Api/Transaction/prosesUpdate';
$route['api-get-transaction-project'] = "Api/Transaction/getListTransaction";
/* End of API SERVICES */


/*   ADMIN PAGE  */
$route['default_controller'] = "Web/Homepage/index";
$route['404_override'] = 'Default/Not_found';
$route['logout'] = 'Default/Auth/logout';
$route['login-admin'] = 'Default/Auth';

/*   route modul dashboard  */
$route['dashboard'] = 'Dashboard/Dashboard/index';

/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';
$route['ubah-password'] = 'Setting/Profile/ubahPassword';
$route['update-profile'] = 'Setting/Profile/update';

/*   route modul member */
$route['master-member'] = 'Master/Member/index';
$route['ajax-member'] = 'Master/Member/ajaxList';
$route['add-member'] = 'Master/Member/add';
$route['save-member'] = 'Master/Member/prosesAdd';
$route['edit-member/(:any)'] = 'Master/Member/edit/$1';
$route['update-member/(:any)'] = 'Master/Member/prosesUpdate/$1';
$route['delete-member'] = 'Master/Member/prosesDelete';
$route['activenon-member'] = 'Master/Member/prosesActivenon';

/*   route modul project */
$route['master-project'] = 'Master/Project/index';
$route['ajax-project'] = 'Master/Project/ajaxList';
$route['add-project'] = 'Master/Project/add';
$route['save-project'] = 'Master/Project/prosesAdd';
$route['edit-project/(:any)'] = 'Master/Project/edit/$1';
$route['update-project/(:any)'] = 'Master/Project/prosesUpdate/$1';
$route['delete-project'] = 'Master/Project/prosesDelete';
$route['openclosed-project'] = 'Master/Project/prosesOpenClosed';

/*   route modul transaction */
$route['management-transaction'] = 'Management/Transaction/index';
$route['ajax-transaction'] = 'Management/Transaction/ajaxList';
$route['manual-transaction'] = 'Management/Transaction/Add';
$route['save-transaction'] = 'Management/Transaction/prosesAdd';
$route['edit-transaction/(:any)'] = 'Management/Transaction/Edit/$1';
$route['update-transaction/(:any)'] = 'Management/Transaction/prosesUpdate/$1';
$route['detail-transaction'] = 'Management/Transaction/Detail';
$route['confirm-transaction/(:any)'] = 'Management/Transaction/confirm/$1';
$route['process-confirm-transaction/(:any)'] = 'Management/Transaction/prosesConfirm/$1';
$route['delete-transaction'] = 'Management/Transaction/prosesDelete';

/*   route report project */
$route['report-project'] = 'Report/ProjectReport/index';
$route['ajax-report-project'] = 'Report/ProjectReport/ajaxList';
$route['print-report-project'] = 'Report/ProjectReport/printReport';

/*   route report transaction */
$route['report-transaction'] = 'Report/TransactionReport/index';
$route['ajax-report-transaction'] = 'Report/TransactionReport/ajaxList';
$route['print-report-transaction'] = 'Report/TransactionReport/printReport';

/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['ajax-user'] = 'Setting/User/ajaxList';
$route['add-user'] = 'Setting/User/add';
$route['save-user'] = 'Setting/User/prosesAdd';
$route['edit-user/(:any)'] = 'Setting/User/Edit/$1';
$route['update-user/(:any)'] = 'Setting/User/prosesUpdate/$1';
$route['delete-user'] = 'Setting/User/prosesDelete';

/*   route modul menu  */
$route['menu'] = 'Setting/Menu/index';
$route['ajax-menu'] = 'Setting/Menu/menuAjax';
$route['save-menu'] = 'Setting/Menu/prosesTambah';
$route['save-sort-menu'] = 'Setting/Menu/saveSort';
$route['icon'] = 'Setting/Menu/icon';
$route['edit-menu/(:any)'] = 'Setting/Menu/edit/$1';
$route['update-menu/(:any)'] = 'Setting/Menu/prosesUpdate/$1';
$route['delete-menu'] = 'Setting/Menu/prosesDelete';

/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/add';
$route['save-grup'] = 'Setting/Grup/prosesTambah';
$route['edit-grup/(:any)'] = 'Setting/Grup/edit/$1';
$route['update-grup/(:any)'] = 'Setting/Grup/prosesUpdate/$1';
$route['delete-grup'] = 'Setting/Grup/prosesDelete';

/*    route modul Akses  */
$route['hak-akses/(:any)'] = 'Setting/Akses/hakAkses/$1';
$route['update-hak-akses/(:any)'] = 'Setting/Akses/updateHakAkses/$1';
/* End of ADMIN PAGE */

/* End of file routes.php */
/* Location: ./application/config/routes.php */