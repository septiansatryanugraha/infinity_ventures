<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends AUTH_Controller
{
    const __tableName = 'tbl_project';
    const __tableId = 'id_project';
    const __folder = 'v_project/';
    const __kode_menu = 'project';
    const __title = 'Project';
    const __model = 'M_project';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajaxList()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_project->getData(1, [], $filter);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->category;
            $row[] = $brand->name;
            $row[] = $brand->currency . ' ' . number_format($brand->allocation, 0, ".", ",");
            $row[] = $brand->currency . ' ' . number_format($brand->allocation_limit_member, 0, ".", ",");
            $row[] = $brand->currency . ' ' . number_format($brand->allocation_remain, 0, ".", ",");
            $row[] = $brand->status;
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->created_date, 'by' => $brand->created_by]);
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->updated_date, 'by' => $brand->updated_by]);

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-project') . "/" . $brand->id_project . "'><i class='fa fa-edit'></i> Edit</a></li>";
            }
            if ($accessEdit->menuview > 0) {
                $label = ($brand->status == "OPEN") ? 'Closed' : 'Open';
                $class = ($brand->status == "OPEN") ? 'glyphicon glyphicon-eye-close' : 'glyphicon glyphicon-eye-open';
                $action .= "    <li><a href='#' class='status-project' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_project . "'><i class='" . $class . "'></i>" . $label . "</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='delete-project' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_project . "'><i class='glyphicon glyphicon-trash'></i> Delete</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Add " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            $data['category'] = $this->M_utilities->getCategory();
            $data['currency'] = $this->M_utilities->getCurrency();
            parent::loadkonten('' . self::__folder . 'add', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $idCategory = trim($this->input->post("id_category"));
        $idCurrency = trim($this->input->post("id_currency"));
        $name = trim($this->input->post("name"));
        $allocation = trim($this->input->post("allocation"));
        $allocation = (int) str_replace(',', '', $allocation);
        $allocationLimitMember = trim($this->input->post("allocation_limit_member"));
        $allocationLimitMember = (int) str_replace(',', '', $allocationLimitMember);
        $priceToken = trim($this->input->post("price_token"));
        $priceToken = str_replace(',', '', $priceToken);
        $tgePrecentage = trim($this->input->post("tge_precentage"));
        $tgePrecentage = str_replace(',', '', $tgePrecentage);
        $vestingAmount = trim($this->input->post("vesting_amount"));
        $vestingAmount = str_replace(',', '', $vestingAmount);
        $vesting = trim($this->input->post("vesting"));
        $cliftAmount = trim($this->input->post("clift_amount"));
        $cliftAmount = str_replace(',', '', $cliftAmount);
        $clift = trim($this->input->post("clift"));
        $fileUpload = $_FILES['project_image'];
        $description = trim($this->input->post("description"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCategory) == 0) {
                $errCode++;
                $errMessage = "Category is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idCurrency) == 0) {
                $errCode++;
                $errMessage = "Currency is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($name) == 0) {
                $errCode++;
                $errMessage = "Name is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($allocation) == 0) {
                $errCode++;
                $errMessage = "Allocation is required.";
            }
        }
        if ($errCode == 0) {
            if ($allocation < 0) {
                $errCode++;
                $errMessage = "Allocation should more than 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($allocationLimitMember) == 0) {
                $errCode++;
                $errMessage = "Allocation Limit Member is required.";
            }
        }
        if ($errCode == 0) {
            if ($allocationLimitMember < 0) {
                $errCode++;
                $errMessage = "Allocation Limit Member should more than 0.";
            }
        }
        if ($errCode == 0) {
            if ($allocationLimitMember > $allocation) {
                $errCode++;
                $errMessage = "Allocation Limit Member should less than Allocation.";
            }
        }
        if ($errCode == 0) {
            if (strlen($priceToken) == 0) {
                $errCode++;
                $errMessage = "Price Token is required.";
            }
        }
        if ($errCode == 0) {
            if ($priceToken < 0) {
                $errCode++;
                $errMessage = "Price Token should more than 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tgePrecentage) == 0) {
                $errCode++;
                $errMessage = "TGE Precentage is required.";
            }
        }
        if ($errCode == 0) {
            if ($tgePrecentage < 0 || $tgePrecentage > 100) {
                $errCode++;
                $errMessage = "TGE Precentage should between 0 - 100 %.";
            }
        }
        if ($errCode == 0) {
            if (strlen($vestingAmount) == 0) {
                $errCode++;
                $errMessage = "Vesting ammount is required.";
            }
        }
        if ($errCode == 0) {
            if ($vestingAmount < 0) {
                $errCode++;
                $errMessage = "Vesting ammount should more than 0.";
            }
        }
        if ($errCode == 0) {
            if (strlen($vesting) == 0) {
                $errCode++;
                $errMessage = "Vesting is required.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] > 1007200) {
                $errCode++;
                $errMessage = "Image should less than 1 Mb.";
            }
        }
        if ($errCode == 0) {
            try {
                if ($vestingAmount == 0) {
                    $vestingPrecentage = (100 - $tgePrecentage) / 100;
                } else {
                    $vestingPrecentage = ((100 - $tgePrecentage) / $vestingAmount) / 100;
                }
                $data = [
                    'id_category' => $idCategory,
                    'id_currency' => $idCurrency,
                    'name' => $name,
                    'allocation' => $allocation,
                    'allocation_limit_member' => $allocationLimitMember,
                    'allocation_remain' => $allocation,
                    'price_token' => $priceToken,
                    'tge_precentage' => $tgePrecentage / 100,
                    'clift' => (strlen($clift) > 0) ? $clift : null,
                    'clift_amount' => (strlen($cliftAmount) > 0) ? ($cliftAmount > 0) ? $cliftAmount : null : null,
                    'vesting' => $vesting,
                    'vesting_amount' => $vestingAmount,
                    'vesting_precentage' => $vestingPrecentage,
                    'description' => $description,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->insert(self::__tableName, $data);
                $id = $this->db->insert_id();
                if (strlen($id) > 0 && $fileUpload['size'] > 0) {
                    if (!is_dir('upload')) {
                        mkdir('./upload', 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu)) {
                        mkdir('./upload/' . self::__kode_menu, 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu . '/' . $id)) {
                        mkdir('./upload/' . self::__kode_menu . '/' . $id, 0777, TRUE);
                    }
                    $config['upload_path'] = "./upload/" . self::__kode_menu . "/" . $id . "/";
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '1024'; //maksimum besar file 1M
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("project_image")) {
                        $imageData = $this->upload->data();
                        $data = [
                            'image' => 'upload/' . self::__kode_menu . '/' . $id . '/' . $imageData['file_name'],
                        ];
                        $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
                    }
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $API_ACCESS_KEY = "AAAAk5WSiS8:APA91bFajIRvAkt4pFZ6_lgvYmjjQyr8-7-N5dCRE1DTW4YiLHsu1H2PdZBJ-hWvkOULIfhX0mGQM2OeXXRK8Cyn8fcDrRBhVRBkVP1razJLh7EpcIsRz6p4DlfATf6umQP_bZsORprF";
            $url = 'https://fcm.googleapis.com/fcm/send';

            $fields = array(
                'to' => '/topics/all',
                'notification' => array(
                    'title' => 'Notifikasi Project Baru',
                    'body' => 'Hallo member ada project baru dengan nama ' . $name . ' di Tanggal ' . $date2 . ' ',
                    'click_action' => 'Notifikasi',
                    "sound" => "Enabled",
                    "priority" => "High",
                    "android_channel_id" => "1000",
                ),
            );

            $fields = json_encode($fields);
            $headers = array(
                'Authorization: key=' . $API_ACCESS_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            $result = curl_exec($ch);
            curl_close($ch);

            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Saved'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_project->selectById($id);
            if ($resultData != null) {
                $checkForeign = $this->M_project->checkForeign($id);
//                if (!$checkForeign) {
                $data['resultData'] = $resultData;
                $data['checkForeign'] = $checkForeign;
                $data['category'] = $this->M_utilities->getCategory();
                $data['currency'] = $this->M_utilities->getCurrency();
                $image = "/assets/dist/img/no-image.png";
                if (file_exists($resultData->image)) {
                    $image = "/" . $resultData->image;
                }
                $data['image'] = $image;
                $data['menuName'] = self::__kode_menu;
                parent::loadkonten('' . self::__folder . 'update', $data);
//                } else {
//                    echo "<script>alert('" . self::__title . " has been in transaction.'); window.location = '" . base_url('master-project') . "';</script>";
//                }
            } else {
                echo "<script>alert('" . self::__title . " invalid.'); window.location = '" . base_url('master-project') . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idCategory = trim($this->input->post("id_category"));
        $idCurrency = trim($this->input->post("id_currency"));
        $name = trim($this->input->post("name"));
        $allocation = trim($this->input->post("allocation"));
        $allocation = str_replace(',', '', $allocation);
        $allocationLimitMember = trim($this->input->post("allocation_limit_member"));
        $allocationLimitMember = str_replace(',', '', $allocationLimitMember);
        $priceToken = trim($this->input->post("price_token"));
        $priceToken = str_replace(',', '', $priceToken);
        $tgePrecentage = trim($this->input->post("tge_precentage"));
        $tgePrecentage = str_replace(',', '', $tgePrecentage);
        $vestingAmount = trim($this->input->post("vesting_amount"));
        $vestingAmount = str_replace(',', '', $vestingAmount);
        $vesting = trim($this->input->post("vesting"));
        $cliftAmount = trim($this->input->post("clift_amount"));
        $cliftAmount = str_replace(',', '', $cliftAmount);
        $clift = trim($this->input->post("clift"));
        $fileUpload = $_FILES['project_image'];
        $description = trim($this->input->post("description"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID is required.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_project->checkForeign($id);
            $checkValid = $this->M_project->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " is invalid.";
            }
        }
        if (!$checkForeign) {
            if ($errCode == 0) {
                if (strlen($idCategory) == 0) {
                    $errCode++;
                    $errMessage = "Category is required.";
                }
            }
            if ($errCode == 0) {
                if (strlen($idCurrency) == 0) {
                    $errCode++;
                    $errMessage = "Currency is required.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($name) == 0) {
                $errCode++;
                $errMessage = "Name is required.";
            }
        }
        if (!$checkForeign) {
            if ($errCode == 0) {
                if (strlen($allocation) == 0) {
                    $errCode++;
                    $errMessage = "Allocation is required.";
                }
            }
            if ($errCode == 0) {
                if ($allocation < 0) {
                    $errCode++;
                    $errMessage = "Allocation should more than 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($allocationLimitMember) == 0) {
                    $errCode++;
                    $errMessage = "Allocation Limit Member is required.";
                }
            }
            if ($errCode == 0) {
                if ($allocationLimitMember < 0) {
                    $errCode++;
                    $errMessage = "Allocation Limit Member should more than 0.";
                }
            }
            if ($errCode == 0) {
                if ($allocationLimitMember > $allocation) {
                    $errCode++;
                    $errMessage = "Amount Allocation Limit Member should less than Amount Allocation.";
                }
            }
            if ($errCode == 0) {
                if ($checkValid->allocation_limit_member != null) {
                    if ($allocationLimitMember > $checkValid->allocation_limit_member) {
                        $errCode++;
                        $errMessage = "Amount Allocation Limit Member should less than Amount before.";
                    }
                }
            }
            if ($errCode == 0) {
                if (strlen($priceToken) == 0) {
                    $errCode++;
                    $errMessage = "Price Token is required.";
                }
            }
            if ($errCode == 0) {
                if ($priceToken < 0) {
                    $errCode++;
                    $errMessage = "Price Token should more than 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($tgePrecentage) == 0) {
                    $errCode++;
                    $errMessage = "TGE Precentage is required.";
                }
            }
            if ($errCode == 0) {
                if ($tgePrecentage < 0 || $tgePrecentage > 100) {
                    $errCode++;
                    $errMessage = "TGE Precentage should between 0 - 100 %.";
                }
            }
            if ($errCode == 0) {
                if (strlen($vestingAmount) == 0) {
                    $errCode++;
                    $errMessage = "Vesting ammount is required.";
                }
            }
            if ($errCode == 0) {
                if ($vestingAmount < 0) {
                    $errCode++;
                    $errMessage = "Vesting ammount should more than 0.";
                }
            }
            if ($errCode == 0) {
                if (strlen($vesting) == 0) {
                    $errCode++;
                    $errMessage = "Vesting is required.";
                }
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] > 1007200) {
                $errCode++;
                $errMessage = "Image should less than 1 Mb.";
            }
        }
        if ($errCode == 0) {
            try {
                if (!$checkForeign) {
                    if ($vestingAmount == 0) {
                        $vestingPrecentage = (100 - $tgePrecentage) / 100;
                    } else {
                        $vestingPrecentage = ((100 - $tgePrecentage) / $vestingAmount) / 100;
                    }
                }
                $data = [
                    'name' => $name,
                    'description' => $description,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                if (!$checkForeign) {
                    $data = array_merge($data, [
                        'id_category' => $idCategory,
                        'id_currency' => $idCurrency,
                        'allocation' => $allocation,
                        'allocation_limit_member' => $allocationLimitMember,
                        'price_token' => $priceToken,
                        'tge_precentage' => $tgePrecentage / 100,
                        'clift' => (strlen($clift) > 0) ? $clift : null,
                        'clift_amount' => (strlen($cliftAmount) > 0) ? ($cliftAmount > 0) ? $cliftAmount : null : null,
                        'vesting' => $vesting,
                        'vesting_amount' => $vestingAmount,
                        'vesting_precentage' => $vestingPrecentage,
                    ]);
                }
                if ($fileUpload['size'] > 0) {
                    if (!is_dir('upload')) {
                        mkdir('./upload', 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu)) {
                        mkdir('./upload/' . self::__kode_menu, 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu . '/' . $id)) {
                        mkdir('./upload/' . self::__kode_menu . '/' . $id, 0777, TRUE);
                    }
                    $config['upload_path'] = "./upload/" . self::__kode_menu . "/" . $id . "/";
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '1024'; //maksimum besar file 1M
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("project_image")) {
                        $imageData = $this->upload->data();
                        $data = array_merge($data, [
                            'image' => 'upload/' . self::__kode_menu . '/' . $id . '/' . $imageData['file_name'],
                        ]);
                    }
                }
                $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Updated'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesOpenClosed()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_project->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['status' => ($checkValid->status == "OPEN") ? "CLOSED" : "OPEN"], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Change Status'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_project->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_project->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Deleted'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
