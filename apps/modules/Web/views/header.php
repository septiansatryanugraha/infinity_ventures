<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Infinity Ventures</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link rel="icon" href="<?= base_url() ?>/assets/web/tambahan/gambar/logo-polantas.png">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/sweetalert/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() . 'assets/web/css/bootstrap.css' ?>">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/font-awesome.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/pages/dashboard.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/pages/faq.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/style.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/public/css/pages/signin.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/web/toastr/toastr.css">
        <link rel='stylesheet' href='<?= base_url(); ?>assets/dist/css/popout.css' type='text/css' media='all'/>
        <script type="text/javascript" src="<?= base_url(); ?>assets/web/sweetalert/sweetAlert.min2.js"></script>
        <script type="text/javascript" src="<?= base_url() . 'assets/web/js/jquery-2.2.3.min.js' ?>"></script>
        <style type="text/css">
            .box {
                position: relative;
                max-width: 600px;
                width: 90%;
                height: 400px;
                background: #fff;
                box-shadow: 0 0 15px rgba(0,0,0,.1);
            }
            /* common */
            .ribbon {
                width: 130px;
                height: 130px;
                overflow: hidden;
                position: absolute;
            }
            .ribbon::before,
            .ribbon::after {
                position: absolute;
                z-index: 10;
                content: '';
                display: block;
                border: 5px solid #2980b9;
            }
            .ribbon span {
                position: absolute;
                display: block;
                width: 200px;
                padding: 10px 0;
                background-color: #3498db;
                box-shadow: 0 5px 10px rgba(0,0,0,.1);
                color: #fff;
                font: 700 18px/1 'Lato', sans-serif;
                text-shadow: 0 1px 1px rgba(0,0,0,.2);
                text-transform: uppercase;
                text-align: center;
            }
            /* top left*/
            .ribbon-top-left {
                top: -10px;
                left: 5px;
            }
            .ribbon-top-left::before,
            .ribbon-top-left::after {
                border-top-color: transparent;
                border-left-color: transparent;
            }
            .ribbon-top-left::before {
                top: 0;
                right: 0;
            }
            .ribbon-top-left::after {
                bottom: 0;
                left: 0;
            }
            .ribbon-top-left span {
                right: -25px;
                top: 30px;
                transform: rotate(-45deg);
            }
            /* top right*/
            .ribbon-top-right {
                top: -10px;
                right: -10px;
            }
            .ribbon-top-right::before,
            .ribbon-top-right::after {
                border-top-color: transparent;
                border-right-color: transparent;
            }
            .ribbon-top-right::before {
                top: 0;
                left: 0;
            }
            .ribbon-top-right::after {
                bottom: 0;
                right: 0;
            }
            .ribbon-top-right span {
                left: -25px;
                top: 30px;
                transform: rotate(45deg);
            }
            /* bottom left*/
            .ribbon-bottom-left {
                bottom: -10px;
                left: -10px;
            }
            .ribbon-bottom-left::before,
            .ribbon-bottom-left::after {
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .ribbon-bottom-left::before {
                bottom: 0;
                right: 0;
            }
            .ribbon-bottom-left::after {
                top: 0;
                left: 0;
            }
            .ribbon-bottom-left span {
                right: -25px;
                bottom: 30px;
                transform: rotate(225deg);
            }
            /* bottom right*/
            .ribbon-bottom-right {
                bottom: -10px;
                right: -10px;
            }
            .ribbon-bottom-right::before,
            .ribbon-bottom-right::after {
                border-bottom-color: transparent;
                border-right-color: transparent;
            }
            .ribbon-bottom-right::before {
                bottom: 0;
                left: 0;
            }
            .ribbon-bottom-right::after {
                top: 0;
                right: 0;
            }
            .ribbon-bottom-right span {
                left: -25px;
                bottom: 30px;
                transform: rotate(-225deg);
            }
            @media only screen (min-width: 1000px) and (max-width: 1900px) {
                .scroll{
                 overflow-y: hidden; /* Hide vertical scrollbar */
                 overflow-x: hidden; /* Hide horizontal scrollbar */
                 height: 600px;
                }
            }

            @media only screen and (max-width: 600px) {
                .scroll{
                 overflow-y: auto; 
                 overflow-x: auto; 
                 height: 500px;
                }
            }
            @media only screen and (max-width: 600px) {
                .logo {
                    margin-left: 120px;
                }
                .wrap .wrap2 input,
                .wrap .ex {
                    display: inline-block;
                }
                .wrap {
                    position: relative;
                    width: 100%;
                    height: 20px;
                    margin-bottom: 30px;
                }
                .ex3 {
                    position: absolute;
                    right: 0;
                    top: 10%;
                    height: 20px;
                    width: 20px;
                    margin-right: 30px;
                    /*opacity: 0;*/
                    font-size: 20px;
                    line-height: 30px;
                    border: 1px solid transparent;
                    text-align: center;
                    cursor: pointer;
                    transition: all 0.8s;
                    display: none;
                }
                .wrap2 .ex {
                    display: inline-block;
                }
                .wrap2 {
                    position: relative;
                    width: 100%;
                    height: 20px;
                }
                .ex2 {
                    position: absolute;
                    right: 0;
                    top: 10%;
                    height: 20px;
                    width: 20px;
                    z-index: 99;
                    /*opacity: 0;*/
                    margin-right: 5px;
                    font-size: 20px;
                    line-height: 30px;
                    border: 1px solid transparent;
                    text-align: center;
                    cursor: pointer;
                    display: none;
                }
            }
        </style>
    </head>

    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container logo"><img src="<?= base_url(); ?>assets/web/images/logo-profil.png" width="130px">
            </div>
        </div>
    </div>

    <?php if (!$this->session->userdata('status') == 1) : ?>
    <?php else: ?> 
        <div class="subnavbar">
            <div class="subnavbar-inner">
                <div class="container">
                    <ul class="mainnav">
                        <li class="<?= $this->uri->segment(1) == 'home-dashboard' ? 'active' : '' ?>" >
                            <a href="<?= site_url(); ?>home-dashboard.html" ><i class="icon-dashboard"></i><span>Dashboard</span></a>
                        </li>
                        <li class="<?= $this->uri->segment(1) == 'home-dashboard-history' ? 'active' : '' ?>" >
                            <a href="<?= site_url(); ?>home-dashboard-history.html" ><i class="fa fa-history"></i><span>My Investment</span></a>
                        </li>
                        <li class="<?= $this->uri->segment(1) == 'home-dashboard-profil' ? 'active' : '' ?>" >
                            <a href="<?= site_url(); ?>home-dashboard-profil.html" ><i class="icon-user"></i><span>Profil</span></a>
                        </li>
                        <li><a class="logout" href="#"><i class="fa fa-sign-out"></i><span>Keluar</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <script type="text/javascript">
        $(".logout").click(function (event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('logout-user'); ?>",
                beforeSend: function () {
                    swal({
                        imageUrl: "<?= base_url(); ?>assets/web/images/ajax-loader.gif",
                        title: "Proses",
                        text: "Tunggu sebentar",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function (data) {
                    swal({
                        type: 'success',
                        title: 'Keluar',
                        text: 'Anda Telah Keluar ',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    setTimeout("window.location='<?= base_url("home-dashboard"); ?>'", 700);
                }
            });
            event.preventDefault();
        });
    </script>
