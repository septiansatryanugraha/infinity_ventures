 
<style>
    #btn_loading {
        display: none;
    }
</style>

<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-update" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-5">
                                    <span class="form-control"><?= $resultData->email; ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" placeholder="Name" id="name" aria-describedby="sizing-addon2" value="<?= $resultData->name; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone" id="phone" aria-describedby="sizing-addon2" value="<?= $resultData->phone; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Wallet Non Exchange</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="wallet" placeholder="Wallet" id="wallet" aria-describedby="sizing-addon2" value="<?= $resultData->wallet; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control" name="password" placeholder="Password" id="password" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-sm-1"></div>
                                <div id="slider">
                                    <img class="img-thumbnail" src="<?= base_url() . $image; ?>" alt="your image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputFoto" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-5">
                                    <input type="file" class="form-control" name="member_image" id="member_image" onchange="return checkLoadImage('member_image')"/>
                                </div>
                                <div class="col-sm-2 space">
                                    <small class="label pull-center bg-red">max 1 Mb </small>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="update" id="update" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                                <a class="klik ajaxify" href="<?= base_url('master-member'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="update" id="update" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Update Data?",
            text: "Are You Sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Update",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?= base_url("update-member") . '/' . $resultData->id; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $("#buka").show();
                $("#btn_loading").hide();
                if (result.status == true) {
                    setTimeout("window.location='<?= base_url("master-member"); ?>'", 450);
                    toastr.success(result.pesan, 'Success', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                } else {
                    toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    return false;
                }
            })
        });
    });
</script>