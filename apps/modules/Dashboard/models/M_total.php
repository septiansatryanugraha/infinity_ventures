<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{
    private $idUser = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
    }

    public function total_member()
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function total_project()
    {
        $this->db->select('*');
        $this->db->from('tbl_project');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function total_transaction()
    {
        $this->db->select('*');
        $this->db->from('tbl_transaction');
        $this->db->where('deleted_date IS NULL');
        $data = $this->db->get();

        return $data->num_rows();
    }
}
