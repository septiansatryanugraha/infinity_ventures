

//untuk notifikasi berhasil

function save_berhasil()
{
    swal({
        position: 'top',
        type: 'success',
        title: 'Data Successfully Saved',
        showConfirmButton: false,
        timer: 1500
    });
}

//untuk notifikasi hapus berhasil

function hapus_berhasil()
{
    swal({
        position: 'top',
        type: 'success',
        title: 'Data Successfully Deleted',
        showConfirmButton: false,
        timer: 1500
    });
}

//untuk notifikasi gagal

function gagal()
{
    swal({
        position: 'top',
        type: 'error',
        title: 'Data Fail to Saved',
        showConfirmButton: false,
        timer: 1000
    });
}

//untuk notifikasi peringatan

function peringatan()
{
    swal({
        position: 'top',
        type: 'warning',
        title: 'Peringatan',
        text: 'No Telp / Username Sudah digunakan Employe lain',
        showConfirmButton: false,
        timer: 2100
    });
}

//untuk notifikasi peringatan

function format()
{
    swal({
        position: 'top',
        title: 'Peringatan',
        text: 'File tidak sesuai dengan format !!!',
        showConfirmButton: false,
        timer: 1500
    });
}

//untuk notifikasi update berhasil

function update_berhasil()
{
    swal({
        position: 'top',
        type: 'success',
        title: 'Data Successfully Updated',
        showConfirmButton: false,
        timer: 1500
    });
}


//untuk notifikasi upload berhasil

function upload_berhasil()
{
    swal({
        position: 'top',
        type: 'success',
        title: 'Data Successfully Uploaded',
        showConfirmButton: false,
        timer: 2500
    });
}


//untuk live gambar ajax slider	

function fileValidation() {
    var fileInput = document.getElementById('gambar');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('maaf masukan gambar dengan format .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('slider').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}



//untuk live gambar ajax profil

function fileFoto() {
    var fileInput = document.getElementById('foto');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('maaf masukan gambar dengan format .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('profile').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}



//untuk live gambar ajax produk

function fileProduk() {
    var fileInput = document.getElementById('gambar');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('maaf masukan gambar dengan format .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('produk').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}


//untuk live gambar detail_produk

function detailproduk() {
    var fileInput = document.getElementById('gambar');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('maaf masukan gambar dengan format .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('detail-produk').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}


function valid1()
{
    var fileInput = document.getElementById("nota_dinas").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("nota_dinas").value = '';
            return false;
        }
        var ukuran = document.getElementById("nota_dinas");
        if (ukuran.files[0].size > 5007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 5MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}


function valid2()
{
    var fileInput = document.getElementById("file_cv").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.doc|\.docx)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .docx", "warning");
            document.getElementById("file_cv").value = '';
            return false;
        }
        var ukuran = document.getElementById("file_cv");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}


function valid3()
{
    var fileInput = document.getElementById("draft").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("draft").value = '';
            return false;
        }
        var ukuran = document.getElementById("draft");
        if (ukuran.files[0].size > 5007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 5MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid4()
{
    var fileInput = document.getElementById("telaah").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.doc|\.docx)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .docx", "warning");
            document.getElementById("telaah").value = '';
            return false;
        }
        var ukuran = document.getElementById("telaah");
        if (ukuran.files[0].size > 5007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 5MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}


function valid5()
{
    var fileInput = document.getElementById("sri").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("sri").value = '';
            return false;
        }
        var ukuran = document.getElementById("sri");
        if (ukuran.files[0].size > 5007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 5MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}


function valid_file()
{
    var fileInput = document.getElementById("files").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            // swal("Peringatan", "File harus format .pdf", "warning");
            toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            document.getElementById("files").value = '';
            return false;
        }
        var ukuran = document.getElementById("files");
        if (ukuran.files[0].size > 507200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 1MB", "warning");
            toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file2()
{
    var fileInput = document.getElementById("files2").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            // swal("Peringatan", "File harus format .pdf", "warning");
            toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            document.getElementById("files2").value = '';
            return false;
        }
        var ukuran = document.getElementById("files2");
        if (ukuran.files[0].size > 507200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 1MB", "warning");
            toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file3()
{
    var fileInput = document.getElementById("files3").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            // swal("Peringatan", "File harus format .pdf", "warning");
            toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            document.getElementById("files3").value = '';
            return false;
        }
        var ukuran = document.getElementById("files3");
        if (ukuran.files[0].size > 507200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 1MB", "warning");
            toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file4()
{
    var fileInput = document.getElementById("files4").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            // swal("Peringatan", "File harus format .pdf", "warning");
            toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            document.getElementById("files4").value = '';
            return false;
        }
        var ukuran = document.getElementById("files4");
        if (ukuran.files[0].size > 507200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 1MB", "warning");
            toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file5()
{
    var fileInput = document.getElementById("files5").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            // swal("Peringatan", "File harus format .pdf", "warning");
            toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            document.getElementById("files5").value = '';
            return false;
        }
        var ukuran = document.getElementById("files5");
        if (ukuran.files[0].size > 507200)  // validasi ukuran size file
        {
            // swal("Peringatan", "File harus maksimal 1MB", "warning");
            toastr.error('File harus maksimal 500 kb', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file6()
{
    var fileInput = document.getElementById("files6").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("files6").value = '';
            return false;
        }
        var ukuran = document.getElementById("files6");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file7()
{
    var fileInput = document.getElementById("files7").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("files7").value = '';
            return false;
        }
        var ukuran = document.getElementById("files7");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_file8()
{
    var fileInput = document.getElementById("files8").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("files8").value = '';
            return false;
        }
        var ukuran = document.getElementById("files8");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_surat1()
{
    var fileInput = document.getElementById("surat_rekomendasi").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("surat_rekomendasi").value = '';
            return false;
        }
        var ukuran = document.getElementById("surat_rekomendasi");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_surat2()
{
    var fileInput = document.getElementById("surat_kerjasama").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("surat_kerjasama").value = '';
            return false;
        }
        var ukuran = document.getElementById("surat_kerjasama");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_surat3()
{
    var fileInput = document.getElementById("surat_patuh").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("surat_patuh").value = '';
            return false;
        }
        var ukuran = document.getElementById("surat_patuh");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}

function valid_surat4()
{
    var fileInput = document.getElementById("surat_rekom").value;
    if (fileInput != '')
    {
        var checkfile = fileInput.toLowerCase();
        if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
            swal("Peringatan", "File harus format .pdf", "warning");
            document.getElementById("surat_rekom").value = '';
            return false;
        }
        var ukuran = document.getElementById("surat_rekom");
        if (ukuran.files[0].size > 1007200)  // validasi ukuran size file
        {
            swal("Peringatan", "File harus maksimal 1MB", "warning");
            ukuran.value = '';
            return false;
        }
        return true;
    }
}