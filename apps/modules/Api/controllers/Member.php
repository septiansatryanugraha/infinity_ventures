<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller
{
    const __tableName = 'tbl_user';
    const __tableId = 'id';
    const __model = 'M_member';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
    }

    public function prosesRegistration()
    {
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $email = trim($post['email']);
        $name = trim($post['name']);
        $password = trim($post['password']);
        $phone = trim($post['phone']);

        $this->db->trans_begin();
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email is required.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Email is not valid.";
            }
        }
        if ($errCode == 0) {
            $checkEmail = $this->M_member->selectByExist(['email' => $email]);
            if ($checkEmail != null) {
                $errCode++;
                $errMessage = "Email is already registered.";
            }
        }
        if ($errCode == 0) {
            if (strlen($name) == 0) {
                $errCode++;
                $errMessage = "Name is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Password is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) < 6) {
                $errCode++;
                $errMessage = "Password should more than 6 characters.";
            }
        }
        if ($errCode == 0) {
            if (strlen($phone) == 0) {
                $errCode++;
                $errMessage = "Phone is required.";
            }
        }
        if ($errCode == 0) {
            $checkPhone = $this->M_member->selectByExist(['phone' => $phone]);
            if ($checkPhone != null) {
                $errCode++;
                $errMessage = "Phone is already registered.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'email' => $email,
                    'name' => $name,
                    'password' => md5($password),
                    'phone' => $phone,
                    'created_date' => $date,
                    'created_by' => $name,
                    'updated_date' => $date,
                    'updated_by' => $name,
                ];
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Registered'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesLogin()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = "";
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $this->db->trans_begin();
        if ($errCode == 0) {
            $email = isset($post['email']) ? $post['email'] : null;
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "email is empty";
            }
        }
        if ($errCode == 0) {
            $password = isset($post['password']) ? $post['password'] : null;
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "password is empty";
            }
        }
        if ($errCode == 0) {
            $checkUser = $this->M_member->login(['email' => $email, 'password' => md5($password)]);
            if ($checkUser == false) {
                $errCode++;
                $errMessage = "Sorry username / password still wrong";
            }
        }
        if ($errCode == "") {
            try {
                $data = [
                    'id_user' => $checkUser->id,
                    'session_key' => md5($email) . md5($password) . md5($date),
                    'created_date' => $date,
                    'created_by' => $email,
                    'updated_date' => $date,
                    'updated_by' => $email,
                ];
                $this->db->insert('api_session', $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $result = ['status' => 'true', 'pesan' => ' Thanks You', 'Profile' => $checkUser, 'sessionKey' => $data['session_key']];
        } else {
            $this->db->trans_rollback();
            $result = ['status' => 'false', 'pesan' => $errMessage];
        }

        echo json_encode($result);
    }

    public function checkSession()
    {
        $errCode = '';
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        if ($errCode == 0) {
            $sessionKey = isset($post['session_key']) ? $post['session_key'] : null;
            if (strlen($sessionKey) == 0) {
                $errCode++;
                $errMessage = "sessionKey is empty";
                $successStatus = '400';
            }
        }
        if ($errCode == 0) {
            $checkSession = $this->M_member->checkSession(['session_key' => $sessionKey]);
            if ($checkSession == null) {
                $errCode++;
                $errMessage = "Session Unauthenticated";
            }
        }
        if ($errCode == 0) {
            $result = ['status' => 'true', 'pesan' => "You're log in", 'id_user' => $checkSession->id_user, 'sessionKey' => $sessionKey];
        } else {
            $result = ['status' => 'false', 'pesan' => $errMessage];
        }

        echo json_encode($result);
    }

    public function prosesLogout()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = "";
        $errMessage = "";

        $post = array_merge($_POST, $_GET);
        $this->db->trans_begin();
        if ($errCode == 0) {
            $sessionKey = isset($post['session_key']) ? trim($post['session_key']) : null;
            $checkSession = $this->M_member->checkSession(['session_key' => $sessionKey]);
            if ($checkSession == null) {
                $errCode++;
                $errMessage = "Session Unauthenticated";
            }
        }
        if ($errCode == "") {
            try {
                $data = [
                    'status' => 0,
                    'updated_date' => $date,
                ];
                $this->db->update('api_session', $data, ['session_key' => $sessionKey]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $result = ['status' => 'true', 'pesan' => ' Logout. Thanks You'];
        } else {
            $this->db->trans_rollback();
            $result = ['status' => 'false', 'pesan' => $errMessage];
        }

        echo json_encode($result);
    }

    public function prosesUpdateMember()
    {
        $where = trim($this->input->post('id'));
        $password = trim($post['password']);

        $this->db->trans_begin();

        if ($password == '') {
            $data = [
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'wallet' => $this->input->post('wallet'),
            ];
        } else {
            $data = [
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'wallet' => $this->input->post('wallet'),
                'password' => md5($password),
            ];
        }

        $result = $this->db->update(self::__tableName, $data, [self::__tableId => $where]);
        if ($this->db->trans_status() === FALSE) {
            $out = ['status' => false, 'pesan' => 'Data Has Failed Updated !'];
        }
        if ($result > 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Updated'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => 'Data Has Failed Updated !'];
        }

        echo json_encode($out);
    }
}
