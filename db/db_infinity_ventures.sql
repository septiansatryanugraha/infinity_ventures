-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2022 at 01:56 AM
-- Server version: 5.7.37-log
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `angkasam_infinity_ventures`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `grup_id` int(10) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `hidden` tinyint(4) DEFAULT '0',
  `cookie` varchar(200) DEFAULT NULL,
  `last_login_user` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(200) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `grup_id`, `username`, `password`, `email`, `nama`, `foto`, `status`, `hidden`, `cookie`, `last_login_user`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES
(1, 1, 'superadmin', '40587bff0e72b6fdbba30c40c95e148a', NULL, 'Superadmin', NULL, 3, 1, NULL, '2022-02-17 01:46:23', NULL, NULL, NULL, NULL, NULL),
(2, 2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 'Admin', NULL, 3, 0, NULL, '2021-04-08 08:54:53', '2021-01-20 19:29:58', 'superadmin', '2021-01-20 19:29:58', 'superadmin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `code`, `name`) VALUES
(1, 'p_1', 'Project One');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ip_address` varchar(45) DEFAULT NULL,
  `data` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `code`, `name`) VALUES
(1, 'USD', 'United States Dollar');

-- --------------------------------------------------------

--
-- Table structure for table `grup`
--

CREATE TABLE `grup` (
  `grup_id` int(10) NOT NULL,
  `nama_grup` varchar(300) DEFAULT NULL,
  `deskripsi` varchar(300) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(200) DEFAULT NULL,
  `can_delete` tinyint(4) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grup`
--

INSERT INTO `grup` (`grup_id`, `nama_grup`, `deskripsi`, `created_date`, `created_by`, `updated_date`, `updated_by`, `can_delete`, `deleted_date`) VALUES
(1, 'Super Administrator', 'Full  Akses', '2021-01-24 22:29:34', 'system', '2022-02-17 01:16:22', 'superadmin', 0, NULL),
(2, 'Administrator', 'Akses Terbatas', '2021-01-24 22:29:34', 'superadmin', '2022-02-17 01:14:31', 'superadmin', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_akses`
--

CREATE TABLE `menu_akses` (
  `id_menuakses` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `grup_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `add` int(11) DEFAULT NULL,
  `edit` int(11) DEFAULT NULL,
  `del` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_akses`
--

INSERT INTO `menu_akses` (`id_menuakses`, `id_menu`, `grup_id`, `view`, `add`, `edit`, `del`) VALUES
(1, 8, 1, 1, 0, 0, 0),
(2, 5, 1, 1, 0, 0, 0),
(3, 2, 1, 1, 0, 0, 0),
(4, 1, 1, 1, 0, 0, 0),
(5, 7, 1, 1, 1, 0, 0),
(6, 6, 1, 1, 0, 1, 1),
(7, 4, 1, 1, 1, 1, 1),
(8, 3, 1, 1, 1, 1, 1),
(9, 10, 1, 1, 1, 1, 1),
(10, 9, 1, 1, 1, 1, 1),
(11, 11, 1, 1, 1, 1, 1),
(12, 8, 2, 1, 0, 0, 0),
(13, 5, 2, 1, 0, 0, 0),
(14, 2, 2, 1, 0, 0, 0),
(15, 1, 2, 1, 0, 0, 0),
(16, 7, 2, 1, 1, 0, 0),
(17, 6, 2, 1, 0, 1, 1),
(18, 4, 2, 1, 1, 1, 1),
(19, 3, 2, 1, 1, 1, 1),
(20, 10, 2, 1, 1, 1, 1),
(21, 9, 2, 1, 1, 1, 1),
(22, 11, 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `status_grup`
--

CREATE TABLE `status_grup` (
  `id_status` int(11) NOT NULL,
  `nama` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_grup`
--

INSERT INTO `status_grup` (`id_status`, `nama`) VALUES
(1, 'Enabled'),
(2, 'Disabled'),
(3, 'Aktif'),
(4, 'Belum Aktif'),
(5, 'Hidden'),
(6, 'Publish'),
(7, 'Non aktif'),
(8, 'Belum Approve'),
(9, 'Approve'),
(10, 'Survey'),
(11, 'Selesai Survey'),
(12, 'Selesai Approve'),
(13, 'Belum Verifikasi'),
(14, 'Verifikasi'),
(15, 'Rekom Bidang'),
(16, 'Tidak Rekom'),
(17, 'Penetapan'),
(18, 'Direvisi');

-- --------------------------------------------------------

--
-- Table structure for table `sys_counter`
--

CREATE TABLE `sys_counter` (
  `sys_counter_id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `counter` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(300) DEFAULT NULL,
  `controller` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `icon` varchar(300) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `kode_menu` varchar(100) DEFAULT NULL,
  `menu_file` varchar(100) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(200) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `controller`, `link`, `icon`, `parent`, `kode_menu`, `menu_file`, `urutan`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES
(1, 'Dashboard', 'Dashboard', 'dashboard', 'fas fa-tachometer-alt', 0, 'admin', 'view', 1, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(2, 'Master', NULL, NULL, 'fas fa-tachometer-alt', 0, 'master', 'view', 2, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(3, 'Member', 'Member', 'master-member', 'fa fa-users', 2, 'member', 'view,add,edit,del', 3, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(4, 'Project', 'Project', 'master-project', 'fa fa-th-list', 2, 'project', 'view,add,edit,del', 4, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(5, 'Management', NULL, NULL, 'fas fa-tachometer-alt', 0, 'management', 'view', 5, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(6, 'Transaction', 'Transaction', 'management-transaction', 'fas fa-tachometer-alt', 5, 'transaction', 'view,edit,del', 6, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(7, 'Balance', 'Balance', 'management-balance', 'fas fa-tachometer-alt', 5, 'balance', 'view,add', 7, '2018-04-20 09:17:58', 'superadmin', '2020-12-24 16:44:09', 'superadmin', NULL),
(8, 'Setting', NULL, NULL, 'fas fa-wrench', 0, 'setting', 'view', 8, '2018-04-20 09:19:49', 'superadmin', '2020-12-29 15:51:49', 'superadmin', NULL),
(9, 'Group', 'Grup', 'user-grup', 'fa fa-users', 8, 'user-grup', 'view,add,edit,del', 9, '2018-04-20 09:23:34', 'superadmin', '2018-04-20 09:54:06', 'superadmin', NULL),
(10, 'Menu', 'Menu', 'menu', 'fa fa-th-list', 8, 'menu-master', 'view,add,edit,del', 10, '2020-01-09 08:08:51', 'superadmin', '2020-01-09 08:09:41', 'superadmin', NULL),
(11, 'User', 'User', 'user', 'fa fa-user', 8, 'user', 'view,add,edit,del', 11, '2018-04-20 10:29:57', 'superadmin', '0000-00-00 00:00:00', 'superadmin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project`
--

CREATE TABLE `tbl_project` (
  `id_project` bigint(20) NOT NULL,
  `id_category` bigint(20) DEFAULT NULL,
  `id_currency` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `allocation` double DEFAULT NULL,
  `allocation_limit_member` double DEFAULT NULL,
  `allocation_remain` double DEFAULT NULL,
  `price_token` double DEFAULT NULL,
  `detail` longtext,
  `image` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_project`
--

INSERT INTO `tbl_project` (`id_project`, `id_category`, `id_currency`, `name`, `allocation`, `allocation_limit_member`, `allocation_remain`, `price_token`, `detail`, `image`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES
(1, 1, 1, 'Project 1', 720000, 100, 719900, 0.025, NULL, 'upload/project/1/1.jpg', '2022-02-17 01:49:19', 'superadmin', '2022-02-17 01:49:19', 'superadmin', NULL),
(2, 1, 1, 'Project 2', 100000, 100, 100000, 0.003, NULL, 'upload/project/2/2.jpg', '2022-02-17 01:49:38', 'superadmin', '2022-02-17 01:49:38', 'superadmin', NULL),
(3, 1, 1, 'Project 3', 460000, 100, 460000, 0.025, NULL, 'upload/project/3/3.jpg', '2022-02-17 01:50:00', 'superadmin', '2022-02-17 01:50:00', 'superadmin', NULL),
(4, 1, 1, 'Project 4', 575000, 100, 575000, 0.025, NULL, 'upload/project/4/4.jpg', '2022-02-17 01:51:30', 'superadmin', '2022-02-17 01:51:30', 'superadmin', NULL),
(5, 1, 1, 'Project 5', 535000, 100, 535000, 0.025, NULL, 'upload/project/5/5.jpg', '2022-02-17 01:51:55', 'superadmin', '2022-02-17 01:51:55', 'superadmin', NULL),
(6, 1, 1, 'Project 6', 465000, 100, 465000, 0.025, NULL, 'upload/project/6/6.jpg', '2022-02-17 01:52:18', 'superadmin', '2022-02-17 01:52:18', 'superadmin', NULL),
(7, 1, 1, 'Project 7', 465000, 100, 464900, 0.025, NULL, 'upload/project/7/1.jpg', '2022-02-17 01:52:40', 'superadmin', '2022-02-17 01:52:40', 'superadmin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_history`
--

CREATE TABLE `tbl_project_history` (
  `id` bigint(20) NOT NULL,
  `id_project` bigint(20) DEFAULT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `ref_table` varchar(50) DEFAULT NULL,
  `ref_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `module` varchar(200) DEFAULT NULL,
  `allocation_in` double DEFAULT NULL,
  `allocation_out` double DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `id_transaction` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `id_project` bigint(20) DEFAULT NULL,
  `allocation` double DEFAULT NULL,
  `detail` longtext,
  `status` enum('PENDING','APPROVE','REJECT') DEFAULT 'PENDING',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`id_transaction`, `id_user`, `id_project`, `allocation`, `detail`, `status`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES
(1, 1, 7, 100, NULL, 'PENDING', '2022-02-17 01:53:42', 'Wahyu Sugiarto', '2022-02-17 01:53:42', 'Wahyu Sugiarto', NULL),
(2, 1, 1, 100, NULL, 'PENDING', '2022-02-17 01:53:49', 'Wahyu Sugiarto', '2022-02-17 01:53:49', 'Wahyu Sugiarto', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` bigint(20) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `detail` longtext,
  `image` varchar(300) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `email`, `name`, `password`, `phone`, `detail`, `image`, `status`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`) VALUES
(1, 'wahyupunk10@gmail.com', 'Wahyu Sugiarto', 'a482f23685e2c09c0b667ae19c1c654e', '+6289675773470', NULL, 'upload/member/1/gtaa.png', 1, '2022-02-17 01:47:30', 'superadmin', '2022-02-17 01:47:30', 'superadmin', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `grup_id` (`grup_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`grup_id`),
  ADD UNIQUE KEY `grup_id` (`grup_id`);

--
-- Indexes for table `menu_akses`
--
ALTER TABLE `menu_akses`
  ADD PRIMARY KEY (`id_menuakses`),
  ADD UNIQUE KEY `id_menuakses` (`id_menuakses`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `grup_id` (`grup_id`);

--
-- Indexes for table `status_grup`
--
ALTER TABLE `status_grup`
  ADD PRIMARY KEY (`id_status`),
  ADD UNIQUE KEY `id_status` (`id_status`);

--
-- Indexes for table `sys_counter`
--
ALTER TABLE `sys_counter`
  ADD PRIMARY KEY (`sys_counter_id`) USING BTREE,
  ADD UNIQUE KEY `counter_id` (`sys_counter_id`) USING BTREE;

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD UNIQUE KEY `id_menu` (`id_menu`);

--
-- Indexes for table `tbl_project`
--
ALTER TABLE `tbl_project`
  ADD PRIMARY KEY (`id_project`) USING BTREE,
  ADD UNIQUE KEY `produk_id` (`id_project`) USING BTREE,
  ADD KEY `kat_produk` (`id_category`) USING BTREE,
  ADD KEY `id_currency` (`id_currency`);

--
-- Indexes for table `tbl_project_history`
--
ALTER TABLE `tbl_project_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_project` (`id_project`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`id_transaction`),
  ADD UNIQUE KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_project` (`id_project`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grup`
--
ALTER TABLE `grup`
  MODIFY `grup_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_akses`
--
ALTER TABLE `menu_akses`
  MODIFY `id_menuakses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `status_grup`
--
ALTER TABLE `status_grup`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sys_counter`
--
ALTER TABLE `sys_counter`
  MODIFY `sys_counter_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_project`
--
ALTER TABLE `tbl_project`
  MODIFY `id_project` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_project_history`
--
ALTER TABLE `tbl_project_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `id_transaction` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
