<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends AUTH_Controller
{
    const __tableName = 'tbl_user';
    const __tableId = 'id';
    const __folder = 'v_member/';
    const __kode_menu = 'member';
    const __title = 'Member';
    const __model = 'M_member';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
        $this->load->model(self::__model);
    }

    public function index()
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajaxList()
    {
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_member->getData(1);

        $data = [];
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = [];
            $row[] = $no;
            $row[] = $brand->name;
            $row[] = $brand->email;
            $row[] = $brand->phone;
            $row[] = $brand->wallet;
            $row[] = ($brand->status > 0) ? 'Active' : 'Nonactive';
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->created_date, 'by' => $brand->created_by]);
            $row[] = parent::loadCreatedUpdatedContent(['datetime' => $brand->updated_date, 'by' => $brand->updated_by]);

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-member') . "/" . $brand->id . "'><i class='fa fa-edit'></i> Edit</a></li>";
            }
            if ($accessEdit->menuview > 0) {
                $label = ($brand->status > 0) ? 'Deactive' : 'Active';
                $class = ($brand->status > 0) ? 'glyphicon glyphicon-remove' : 'glyphicon glyphicon-ok';
                $action .= "    <li><a href='#' class='status-member' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id . "'><i class='" . $class . "'></i>" . $label . "</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='delete-member' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id . "'><i class='glyphicon glyphicon-trash'></i> Delete</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Add " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            parent::loadkonten('' . self::__folder . 'add', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $email = trim($this->input->post("email"));
        $name = trim($this->input->post("name"));
        $phone = trim($this->input->post("phone"));
        $password = trim($this->input->post("password"));
        $wallet = trim($this->input->post("wallet"));
        $fileUpload = $_FILES['member_image'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($email) == 0) {
                $errCode++;
                $errMessage = "Email is required.";
            }
        }
        if ($errCode == 0) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errCode++;
                $errMessage = "Email is not valid.";
            }
        }
        if ($errCode == 0) {
            $checkEmail = $this->M_member->selectByExist(['email' => $email]);
            if ($checkEmail != null) {
                $errCode++;
                $errMessage = "Email is already registered.";
            }
        }
        if ($errCode == 0) {
            if (strlen($name) == 0) {
                $errCode++;
                $errMessage = "Name is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($phone) == 0) {
                $errCode++;
                $errMessage = "Phone is required.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_member->selectByExist(['phone' => $phone], $id);
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " is exist.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "Password is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) < 6) {
                $errCode++;
                $errMessage = "Password should more than 6 characters.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] > 1007200) {
                $errCode++;
                $errMessage = "Image should less than 1 Mb.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'email' => $email,
                    'name' => $name,
                    'phone' => $phone,
                    'password' => md5($password),
                    'wallet' => $wallet,
                    'status' => 1,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                $this->db->insert(self::__tableName, $data);
                $id = $this->db->insert_id();
                if (strlen($id) > 0 && $fileUpload['size'] > 0) {
                    if (!is_dir('upload')) {
                        mkdir('./upload', 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu)) {
                        mkdir('./upload/' . self::__kode_menu, 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu . '/' . $id)) {
                        mkdir('./upload/' . self::__kode_menu . '/' . $id, 0777, TRUE);
                    }
                    $config['upload_path'] = "./upload/" . self::__kode_menu . "/" . $id . "/";
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '1024'; //maksimum besar file 1M
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("member_image")) {
                        $imageData = $this->upload->data();
                        $data = [
                            'image' => 'upload/' . self::__kode_menu . '/' . $id . '/' . $imageData['file_name'],
                        ];
                        $result = $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
                    }
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Updated'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_member->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['menuName'] = self::__kode_menu;
                $image = "/assets/dist/img/no-image.png";
                if (file_exists($resultData->image)) {
                    $image = "/" . $resultData->image;
                }
                $data['image'] = $image;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('Member tidak tersedia.'); window.location = '" . base_url('master-member') . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";
        $name = trim($this->input->post("name"));
        $phone = trim($this->input->post("phone"));
        $wallet = trim($this->input->post("wallet"));
        $fileUpload = $_FILES['member_image'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_member->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " invalid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($name) == 0) {
                $errCode++;
                $errMessage = "Name is required.";
            }
        }
        if ($errCode == 0) {
            if (strlen($phone) == 0) {
                $errCode++;
                $errMessage = "Phone is required.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_member->selectByExist(['phone' => $phone], $id);
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " is exist.";
            }
        }
        if ($errCode == 0) {
            if (strlen($password) != 0) {
                if (strlen($password) < 6) {
                    $errCode++;
                    $errMessage = "Password should more than 6 characters.";
                }
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] > 1007200) {
                $errCode++;
                $errMessage = "Image should less than 1 Mb.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = [
                    'name' => $name,
                    'phone' => $phone,
                    'wallet' => $wallet,
                    'updated_date' => $date,
                    'updated_by' => $username,
                ];
                if (strlen($password) > 0) {
                    $data = array_merge($data, [
                        'password' => md5($password),
                    ]);
                }
                if ($fileUpload['size'] > 0) {
                    if (!is_dir('upload')) {
                        mkdir('./upload', 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu)) {
                        mkdir('./upload/' . self::__kode_menu, 0777, TRUE);
                    }
                    if (!is_dir('upload/' . self::__kode_menu . '/' . $id)) {
                        mkdir('./upload/' . self::__kode_menu . '/' . $id, 0777, TRUE);
                    }
                    $config['upload_path'] = "./upload/" . self::__kode_menu . "/" . $id . "/";
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '1024'; //maksimum besar file 1M
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload("member_image")) {
                        $imageData = $this->upload->data();
                        $data = array_merge($data, [
                            'image' => 'upload/' . self::__kode_menu . '/' . $id . '/' . $imageData['file_name'],
                        ]);
                    }
                }
                $this->db->update(self::__tableName, $data, [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Updated'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesActivenon()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_member->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['status' => ($checkValid->status > 0) ? 0 : 1], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Change Status'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST['id'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_member->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_member->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, ['deleted_date' => date('Y-m-d H:i:s')], [self::__tableId => $id]);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = ['status' => true, 'pesan' => ' Data Successfully Deleted'];
        } else {
            $this->db->trans_rollback();
            $out = ['status' => false, 'pesan' => $errMessage];
        }

        echo json_encode($out);
    }
}
