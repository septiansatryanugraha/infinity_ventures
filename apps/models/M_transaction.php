<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaction extends CI_Model
{
    const __tableName = 'tbl_transaction';
    const __tableId = 'id_transaction';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $arrWhere = [], $filter = [])
    {
        $idProject = isset($filter['id_project']) ? $filter['id_project'] : null;
        $idUser = isset($filter['id_user']) ? $filter['id_user'] : null;
        $status = isset($filter['status']) ? $filter['status'] : null;
        $tanggalAwal = isset($filter['tanggal_awal']) ? $filter['tanggal_awal'] : null;
        $tanggalAkhir = isset($filter['tanggal_akhir']) ? $filter['tanggal_akhir'] : null;
        $allDate = isset($filter['all_date']) ? $filter['all_date'] : null;

        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_project.name AS project,
                tbl_project.image AS image,
                tbl_project.description AS description,
                tbl_user.email AS email,
                tbl_user.name AS member,
                tbl_user.wallet AS wallet,
                currency.code AS currency
                FROM " . self::__tableName . "
                LEFT JOIN tbl_project ON tbl_project.id_project = " . self::__tableName . ".id_project
                LEFT JOIN tbl_user ON tbl_user.id = " . self::__tableName . ".id_user
                LEFT JOIN currency ON currency.id = tbl_project.id_currency
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($idProject) > 0 && $idProject != 'all') {
            $sql .= " AND " . self::__tableName . ".id_project = '{$idProject}'";
        }
        if (strlen($idUser) > 0 && $idUser != 'all') {
            $sql .= " AND " . self::__tableName . ".id_user = '{$idUser}'";
        }
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND " . self::__tableName . ".status = '{$status}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".updated_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".updated_date <= '{$tanggalAkhir}'";
            }
        }
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_project.name AS project,
                tbl_project.image AS image,
                tbl_project.description AS description,
                tbl_user.email AS email,
                tbl_user.name AS member,
                tbl_user.wallet AS wallet
                FROM " . self::__tableName . "
                LEFT JOIN tbl_project ON tbl_project.id_project = " . self::__tableName . ".id_project
                LEFT JOIN tbl_user ON tbl_user.id = " . self::__tableName . ".id_user
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByExist($arrWhere = [], $id = null)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if ($id != null) {
            $sql .= " AND " . self::__tableId . " <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    function loadData($limit, $start, $arrWhere = [])
    {
        $this->db->select("*");
        $this->db->from(self::__tableName);
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->order_by("updated_date", "DESC");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query;
    }
}
