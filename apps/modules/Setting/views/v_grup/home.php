<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="form-group ">
                <div class="col-md-4" style="margin-left: 0px; margin-bottom: 10px;">
                    <?php if ($accessAdd > 0) { ?>
                        <a class="klik ajaxify" href="<?= site_url('add-grup'); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Add Data</button></a>
                    <?php } ?>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Grup</th>
                                <th>Modul Kategori</th>
                                <th>Menu Akses</th>
                                <th>Deskripsi</th>
                                <th style="width: 100px;">Dibuat</th>
                                <th style="width: 100px;">Diubah</th>
                                <th style="width: 45px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($dataWarna as $key => $data) {
                                $loop = $data->grup_id;
                                $dataakses = $this->M_grup->aksesGrup($loop);

                                ?>
                                <tr>
                                    <td><?= $key + 1; ?></td>
                                    <td><?= $data->nama_grup; ?></td>
                                    <td>
                                        <?php
                                        foreach ($dataakses as $data1) {
                                            if ($data1->view == 1) {
                                                echo '' . $data1->menuparent . '<br>';
                                            }
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        foreach ($dataakses as $data1) {
                                            if ($data1->view == 1) {
                                                echo '<li>' . $data1->nama_menu . '</li>';
                                            }
                                        }

                                        ?>
                                    </td>
                                    <td><?= $data->deskripsi; ?></td>
                                    <td><?= AUTH_Controller::loadCreatedUpdatedContent(['datetime' => $data->created_date, 'by' => $data->created_by]); ?></td>
                                    <td><?= AUTH_Controller::loadCreatedUpdatedContent(['datetime' => $data->updated_date, 'by' => $data->updated_by]); ?></td>
                                    <td>
                                        <div class='btn-group'>
                                            <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>
                                            <ul class='dropdown-menu align-left pull-right'>
                                                <?php if ($data->grup_id != 1) { ?>
                                                    <li><a class="ajaxify" href="<?= base_url('edit-grup') . '/' . $data->grup_id; ?>"><i class="fa fa-edit"></i> Ubah</a></li>
                                                <?php } ?>
                                                <li><a class="ajaxify" href="<?= base_url('hak-akses') . '/' . $data->grup_id; ?>"><i class="fa fa-cogs"></i> Pengaturan Akses</a></li>
                                                <?php if ($data->grup_id != 1 && $data->can_delete > 0) { ?>
                                                    <li><a href='#' class='hapus-grup' data-toggle='tooltip' data-placement='top' data-id="<?= $data->grup_id; ?>"><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>
                                                    <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //untuk load data table ajax	
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        //datatables
        table = $('#table').DataTable({
            "aLengthMenu": [[10, 50, 75, 100, 150, -1], [10, 50, 75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 10,
            oLanguage: {
                "sSearch": "<i class='fa fa-search fa-fw'></i> Cari : ",
                "sEmptyTable": "No data found in the server",
                sProcessing: "<img src='<?php base_url(); ?>assets/dist/img/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Tidak ada data di server",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                },
            },
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    });

    $(document).on("click", ".hapus-grup", function () {
        var grup_id = $(this).attr("data-id");
        swal({
            title: "Deleted Data?",
            text: "Are you sure deleted this data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Delete",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?= base_url('delete-grup'); ?>",
                data: "grup_id=" + grup_id,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + grup_id + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        hapus_berhasil();
                        setTimeout("window.location='<?= base_url("user-grup"); ?>'", 450);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                }
            });
        });
    });
</script>